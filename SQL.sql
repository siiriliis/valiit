﻿--See on lihtne hello world teksti päring, mis tagastab ühe rea ja ühe veeru. 
--Veerul puudub pealkiri. Selles veerus ja reas saab olema tekst Hello World
SELECT 'Hello World'; 

--Täisarvu saab küsida ilma '' märkideta
SELECT 3;

SELECT 'raud' + 'tee' -- ei tööta PostgreSQL'is. Näiteks MSSgl'is töötab

--standard CONCAT töötab kõigis erinevates SQL serverites
SELECT CONCAT('raud','tee')

--Kui tahan erinevaid veerge, siis panen väärtustele koma vahele
SELECT 'Peeter', 'Paat', 23, 75.45, 'Blond'
--AS'iga saab määrata veeru nime (AS on standard). Selle saab ka ära jätta.
SELECT 'Peeter' AS Eesnimi, 
		'Paat'AS Perekonnanimi, 
		23 AS vanus, 
		75.45 AS kaal, 
		'Blond' AS juuksevärv
-- Tagastab praeguse kuupäeva, kellaaja with time zone (vaikimisi formaadis)
SELECT NOW()

--Kui tahan konkreetset osa sellest, näiteks aastat või kuud
SELECT date_part ('year', NOW())
-- Kui tahan mingit kuupäeva osa ise etteantud kuupäevast
SELECT date_part('month',TIMESTAMP '2019-01-01')
SELECT date_part('month',Date '2019-10-01')
-- Kui tahan kellajast saada näiteks minuteid
SELECT date_part('minutes',TIME '10:10')
-- Kuupäeva teisendamine Eesti süsteemi
--Kui tahan eesti formaadis kellaaega ja kuupäeva
SELECT to_char(NOW(), 'HH24:mi:ss DD.MM.YYYY')
SELECT to_char(NOW(), 'hh24:mi:ss dd.mm.yyyy') -- ei ole vahet kas suur või väike täht
--Interval laseb lisada või eemaldada mingit ajaühikut
--tänane kuupäev - 2 päeva tagasi
SELECT to_char(NOW()+ interval '2 days ago', 'hh24:mi:ss dd.mm.yyyy') 
 -- lisab kaks päeva
SELECT to_char(NOW()+ interval '2 days', 'hh24:mi:ss dd.mm.yyyy')
SELECT TIMESTAMP '2000-01-01' + interval '2 days'
--2 millenniumit tagasi
SELECT to_char(NOW()- interval '2 millenniums', 'hh24:mi:ss dd.mm.yyyy')
SELECT to_char(NOW()+ interval '2 centuries 3 years 2 months 1 week 3 days 4 seconds', 'hh24:mi:ss dd.mm.yyyy')

--andmebaasi loomine:
CREATE DATABASE

--tabeli loomine:
CREATE TABLE student (
	-- id, tüüp, 
	-- serial => tüübiks on int, mis suureneb ühe võrra,
	-- PRIMARY KEY => (primaarvõti), tähendab, et see on unikaalne väli tabelis
	id serial PRIMARY KEY, -- see tuleb ALATI teha
	first_name varchar(64) NOT NULL, -- EI TOHI tühi olla
	last_name varchar(64)NOT NULL,
	height int NULL, -- TOHIB tühi olla
	weight numeric(5,2)NULL,
	birthday date NULL
);
--Tabelist kõikide ridade kõikide veergude küsimine
-- * siin tähendab, et anna kõik veerud
SELECT * FROM student
-- eesnime saamiseks first_name
SELECT first_name FROM student
SELECT first_name, last_name FROM student
--ilusam vormistus oleks nii:
--muudeme samal ajal ka first_name to eesnimi jne
SELECT 
	first_name AS eesnimi, 
	last_name AS perekonnanimi
FROM 
	student;
-- leiame vanuse kui meil on teada vaid sünnipäev
SELECT 
	first_name AS eesnimi, 
	last_name AS perekonnanimi,
	--Kuidas saada vanus?
	date_part('year',NOW()) - date_part('year', birthday) AS vanus 
	
FROM 
	student;
	
-- prindime eesnime ja perekonnanime
SELECT CONCAT (first_name, ' ', last_name)AS täisnimi FROM student;
	
-- Char (n)-iga eraldatakse mälust n arv mälu… ja varchar(n)-iga eraldatakse mälust 
-- nii palju kui vaja on. Nt kui sõna on 6 tähte pikk, siis char(20) 
-- võtab mälust 20 kohta, varchar(20) võtab 6.
-- char(n) on hea kasutada, kui pikkus on teada (nt isikukood).


-- Date -> hoiab kuupäeva
-- Time -> hoiab kellaaega
-- TIMESTAMP -> hoiab kuupäeva ja kellaaega
-- TIMESTAMPTZ -> hoiab koos time zone-iga

-- Kui tahan filtreerida/otsida mingi tingimuse järgi
--anna mulle kõik kasutajad, kelle pikkus on 180 cm
SELECT 	
	* 
FROM 
	student
WHERE 	
	height = 180;
	
-- Kui on kaks tingimust
SELECT 	
	* 
FROM 
	student
WHERE 	
	first_name = 'Peeter'
AND 
	last_name = 'Tamm';
	
--Küsi tabelist eesnime ja perekonnanime järgi 
-- mingi Peeter Tamm ja mingi Mari Maasikas
SELECT 	
	* 
FROM 
	student
WHERE 	
	first_name = 'Peeter'
	AND 
	last_name = 'Tamm'
	OR
	first_name = 'Mari'
	AND 
	last_name = 'Maasikas'
	
--Anna mulle õpilased, kelle pikkus jääb 170 - 180 cm vahele
SELECT 	
	* 
FROM 
	student
WHERE 	
	height > 170 AND height < 180;
	
--Anna mulle õpilased, kes on pikemad kui 170cm või lühemad kui 150cm
SELECT 	
	* 
FROM 
	student
WHERE 	
	height > 170 OR height < 150;
	
-- Anna mulle õpilased, kellel on sünnipäev aprillis
SELECT 	
	* 
FROM 
	student
WHERE 	
	date_part('month', birthday) = 4
	
-- Anna mulle õpilaste eesnimi ja pikkus, kellel on sünnipäev aprillis
SELECT 	
	first_name,
	height
FROM 
	student
WHERE 	
	date_part('month', birthday) = 4
	EXTRACT(MONTH FROM birthday) = 04; --nii töötab ka
	
-- anna mulle õpilased, kelle middle_name on null(määramata)
-- NULL-iga võrdlemine on IS/IS NOT
SELECT 	
	*
FROM 
	student
WHERE 	
	middle_name IS NULL / IS NOT NULL
	
-- anna mulle õpilased, kelle pikkus ei ole 180cm
--standard lahendus on:  <>  või !=
SELECT 	
	*
FROM 
	student
WHERE 	
	height != 180 
	(height <> 180)
	(NOT height = 180)
	
-- Anna mulle õpilased, kelle pikkus on 160, 165 või 175
SELECT 	
	*
FROM 
	student
WHERE 	
	height IN (160,165,175)
	
-- Anna mulle õpilased, kelle eesnimi on Peeter, Mari või Kati
SELECT 	
	*
FROM 
	student
WHERE 	
	first_name IN ('Peeter', 'Mari', 'Kati')
	
-- Anna mulle õpilased, kelle eesnimi EI OLE Peeter, Mari või Kati
SELECT 	
	*
FROM 
	student
WHERE 	
	NOT first_name IN ('Peeter', 'Mari', 'Kati')
	
--Anna mulle õpilased, kelle sünnikuupäev on kuu esimene, neljas või seitsmes päev
SELECT 	
	*
FROM 
	student
WHERE 	
	date_part('day', birthday) IN (1, 4, 7)
	
--Kõik WHERE võrdlused jätavad välja NULL väärtusega read
SELECT 	
	*
FROM 
	student
WHERE 	
	height > 0 OR height <= 0 -- annab nulli siis kui = > OR height IS NULL
	
-- Kuidas ma saan järjestada asju

--Anna mulle õpilased pikkuse järjekorras: ORDER BY (nullid lähevad lõppu)
SELECT 	
	*
FROM 
	student
ORDER BY
	height

--Anna mulle õpilased pikkuse järjekorras lühemast pikkemaks.
--Kui pikkused on võrdsed, järjesta kaalu järgi
SELECT 	
	*
FROM 
	student
ORDER BY
	height, weight
	
-- Tagurpidi järjekorras, suuremast väiksemaks, siis lisandub sõna DESC(Descending)
-- ASC (Ascending) on vaikeväärtus väiksemast suuremaks (seda ei pea kirjutama)
SELECT 	
	*
FROM 
	student
ORDER BY
	first_name DESC, last_name DESC
	
-- Anna mulle vanuse järjekorras vanemast nooremaks 
-- õpilaste pikkused, mis jäävad 160 ja 170 vahele
SELECT 	
	height
FROM 
	student
WHERE
	height >= 160 AND height < 170
ORDER BY
	birthday
	
-- Kuidas ma saan otsida sõnaosa
-- nt otsi K tähega õpilasi
--WHERE ... LIKE...'K%'
-- %k sõna lõppeb k tähega
-- %k% sõna sees on k täht
-- k% sõna algab k tähega
-- Kui tahan otsida sõna seest mingit sõnaühendit
SELECT 	
	*
FROM 
	student
WHERE
	first_name LIKE 'Ka%' -- eesnimi algab Ka-ga
	OR first_name LIKE '%ee%'-- eesnimi sisaldab ee
	OR first_name LIKE '%ri'-- eesnimi lõppeb ri-ga
	
-- Lisame studenti
-- INSERT INTO
INSERT INTO student
	(first_name,last_name, height, weight ,birthday, middle_name)
VALUES
	('Alar', 'Allikas', 172, 80.55, '1980-03-14', NULL),
	('Tiiu', 'Tihane', 171, 53.4, '1994-03-12', 'Tiia'),
	('Marta', 'Maasikas', 166, 56.55, '1984-05-22', 'Marleen')
INSERT INTO student
	(last_name, first_name)
VALUES
	('Aabits', 'Alar')
	
-- Tabelis kirje muutmine. Uuendame andmeid
-- UPDATE lausega peab olema ETTEVAATLIK
-- ALATI peab kasutama WHERE-i lause lõpus
UPDATE 
	student
SET
	height =172
	weight = 62
WHERE	
	first_name = 'Mari' AND last_name = 'Maasikas'
	
-- Muuda kõigi õpilaste pikkust 1 võrra suuremaks
UPDATE 
	student
SET
	height = height+1
	
-- Suurenda hiljem kui 1990 sündinud õpilatel sünnipäeva ühe päeva võrra
UPDATE 
	student
SET
	birthday = birthday + interval '1 days'
WHERE
	date_part('year', birthday) > 1990
	
UPDATE 
	student
SET
	birthday = birthday + 0000-01-00
WHERE
	date_part('year', birthday) > 1990
	
--Kustutamisel olla ettevaatlik. ALATI KASUTA WHERE'i
DELETE FROM
	student
WHERE
	id > 10
	
-- CRUD operations
-- CREATE (INSERT), READ (SELECT), UPDATE, DELETE

--Loo uus tabel loan, millel on väljad: 
--amount (reaalarv st komaga), start_date, due_date, student_id
CREATE TABLE loan (
	id serial PRIMARY KEY,
	amount numeric (11,2) NOT NULL,
	start_date date NOT NULL,
	due_date date NOT NULL,
	student_id int NOT NULL
);
-- lisa neljale õpilase laenud, kahele neist lisa veel üks laen
INSERT INTO loan
	(amount, start_date, due_date, student_id)
VALUES
	(300, NOW(), '2020-12-01', 6),
	(25, NOW() , '2019-10-01', 7),
	(3000, '2019-01-15', '2021-11-07', 8),
	(45000, '2015-06-05', '2030-06-05', 4),
	(500, '2018-12-20', '2019-12-05', 6),
	(600, '2018-06-01', NOW(), 4)
	
-- Anna mulle kõik õpilased koos oma laenudega
-- kelle laen on suurem kui 500, järjesta laenu summa järgi
-- JOIN millele, midagi ette ei kirjuta on vaikimisi INNER JOIN, 
-- sõna INNER võib ära jätta. 
-- INNER JOIN on selline tabelite liitmine, kus liidetakse ainult need read, 
-- kus on võrdsed student.id = loan.student_id
-- ehk need read, kus tabelite vahel on seos. Ülejäänud read ignoreeritakse.
SELECT
	* 
	-- loan.*
	--student.first_name,
	--student.last_name,
	--loan.amount
FROM
	student
JOIN
	loan
	ON student.id = loan.student_id -- ON ütleb, mis koha pealt
WHERE 
	loan.amount >= 500
ORDER BY 
	loan.amount DESC
	
-- kuidas liita kolm tabelit?

-- Loo uus tabel loan_type, milles väljad name ja description
CREATE TABLE loan_type (
	id serial PRIMARY KEY,
	name varchar(30)NOT NULL,
	description varchar(500) NULL
);

-- Lisa tabelisse loan column
-- ALTER TABLE, ADD COLUMN
-- Uue välja lisamine olemasolevale tabelile
ALTER TABLE loan
ADD COLUMN loan_type_id int

--Tabeli kustutamine
DROP TABLE student;

-- Lisame laenutüübid tabelisse loan_type
INSERT INTO loan_type 
	(name, description)
VALUES
	('Õppelaen', 'See on väga hea laen'),
	('SMS laen', 'See on väga väga halb laen'),
	('Väikelaen', 'See on kõrge intressiga laen'),
	('Kodulaen', 'See on madala intressiga laen')
	
--liida kolm tabelit, kolme tabeli INNER JOIN
SELECT
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
-- võib ka teises järjestuses seda teha
SELECT
	s.first_name, s.last_name, l.amount, lt.name
FROM
	loan AS l
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
JOIN
	student AS s
	ON s.id = l.student_id
	
-- LEFT JOIN => võtab joini esimesest (vasakpoolsest tabelist (student tabelist)) kõik read 
-- ning teises (paremas)tabelis näidatakse puuduvatel kohtadel NULL
-- saame tabelis kuvada ka neid, kes ei võtnud laenu
SELECT
	s.first_name, s.last_name, l.amount
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
-- kõik laenud RIGHT JOIN
SELECT
	s.first_name, s.last_name, l.amount
FROM
	student AS s
RIGHT JOIN
	loan AS l
	ON s.id = l.student_id
-- kolme tabeli liitmine, 
-- nii et võtaks kõik studentid ( ka siis kui neil laenu pole)
-- LEFT JOIN puhul ON järjekord väga OLULINE
SELECT
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
LEFT JOIN 
	loan_type AS lt
	ON lt.id = l.loan_type_id
	
-- CROSS JOIN
-- Annab kõik kombinatsioonid kahe tabeli vahel
-- tekitab ristkorrutise 
-- WHERE jätab endaga korrutamata
SELECT
	s.first_name, s.last_name, st.first_name, st.last_name
FROM
	student AS s
CROSS JOIN
	student AS st
WHERE 
	s.first_name != st.first_name

--FULL OUTER JOIN on sama, mis LEFT JOIN + RIGHT JOIN
SELECT
	s.first_name, s.last_name, l.amount
FROM
	student AS s
FULL OUTER JOIN
	loan AS l
	ON s.id = l.student_id
	
-- Anna mulle kõigi kasutajate perekonnanimed, kes võtsid SMS laenu
-- ja kelle laenu kogus on üle 100 euro. 
-- Tulemused järjesta laenu võtja vanuse järgi väiksemast suuremaks
SELECT
	s.last_name
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
WHERE 
	lt.name = 'SMS laen' AND l.amount > 100
ORDER BY
	s.birthday

-- AGGREGATE funktsioonid
-- Agregaatfunktsiooni selectis välja kutsudes 
-- kaob võimalus samas select lauses küsida mingit muud välja tabelist, 
-- sest agregaatfunktsiooni tulemus on alati ainult 1 number 
-- ja seda ei saa kuidagi näidata koos väljadega, mida võib olla mitu rida

--KESKMISE LEIDMINE	
-- Leiame õpilaste keskmise pikkuse
-- ei arvesta NULL
-- ROUND -> saan täpsustada komakohad
SELECT
	ROUND(AVG (height),3)
FROM
	student
	
-- Kui palju on üks õpilane keskmiselt laenu võtnud 
--(arvestades ka neid, kes ei ole laenu võtnud)
SELECT
	ROUND(AVG(COALESCE(loan.amount, 0)),2) 
	-- COALESCE teeb NULL väärtuse 0-ks, ehk loeb tabelist ka neid, 
	-- kellel laenu pole (amount on NULL)
	-- pm võin ka muu nr sinna kirjutada
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
	
-- Leia min/max laenusumma ja ümarda kahe komakohani ning nimeta 
-- Kui nimetamisel on rohkem kui üks sõna, siis kasuta jutumärke
-- ümardamiseks on meetod ROUND 
SELECT
	ROUND(AVG(COALESCE(loan.amount, 0)),2) AS "Keskmine laenusumma",
	ROUND(MIN(loan.amount),2) AS "Min laenusumma",
	ROUND(MAX(loan.amount),2) AS "Max laenusumma"
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
	
-- COUNT()
-- Loe ridade arv
-- Loe laenude arv
-- Mitmel õpilasel on pikkus kirjas
SELECT
	COUNT(*) AS "Kõikide ridade arv",
	COUNT(loan.amount) AS "Laenude arv", -- jäetakse välja read, kus loan.amount on NULL
	COUNT (student.height) AS "Mitmel õpilasel on pikkus kirjas"
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
	
-- GRUPEERIMINE
-- GROUP BY

-- Leia laenusummad grupeerituna õpilase kaupa
-- Kasutatdes GROUP BY jäävad select päringu jaoks alles vaid need väljad, 
-- mis on GROUP BY's ära toodud (s.first_name, s.last_name).
-- Teisi välju saab ainult kasutada agragaatfunktsioonide sees.
SELECT 
	s.first_name, 
	s.last_name, 
	SUM(l.amount),
	ROUND(AVG(l.amount),2),
	MIN(l.amount),
	MAX(l.amount)
FROM
	student AS s
JOIN 
	loan AS l
	ON s.id =l.student_id
GROUP BY
	s.first_name, s.last_name

-- sama, mis eelmine, aga anna ka need, kellel pole laenu (LEFT JOIN)
SELECT 
	s.first_name, 
	s.last_name, 
	SUM(l.amount),
	ROUND(AVG(l.amount),2) AS "Keskmine laenusuma ",
	MIN(l.amount),
	MAX(l.amount)
FROM
	student AS s
LEFT JOIN 
	loan AS l
	ON s.id =l.student_id
GROUP BY
	s.first_name, s.last_name
ORDER BY
	s.first_name

-- Anna mulle laenude, mis ületavad 1000, summad sünniaastate järgi	
SELECT 
	date_part('year', s.birthday), 
	s.first_name,
	SUM(l.amount)
FROM
	student AS s
LEFT JOIN 
	loan AS l
	ON s.id =l.student_id
GROUP BY
	date_part('year', s.birthday), s.first_name
HAVING 
	date_part('year', s.birthday) IS NOT NULL
	AND SUM(l.amount) > 1000 -- anna ainult need read, kus summa oli suurem kui 1000
	AND COUNT (l.amount) = 2 -- anna ainult need read, kus laene oli 2
-- HAVING on nagu WHERE, aga peale GROUP BY kasutamist
-- Filtreerimisel saab kasutada ainult neid välju, mis on 
-- GROUP BY's ja agregaatfunktsioone
ORDER BY
	date_part('year', s.birthday)

-- Anna mulle laenude summa laenutüüpide järgi	
SELECT 
	lt.name,
	SUM(l.amount)
FROM
	loan AS l
LEFT JOIN 
	loan_type AS lt
	ON lt.id =l.loan_type_id
GROUP BY
	lt.name
ORDER BY
	lt.name
	
-- Anna mulle laenude summa grupeerituna õpilase 
-- ning laenu tüübi kaupa
SELECT 
	s.first_name,
	s.last_name,
	lt.name,
	SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY 
	s.first_name, s.last_name, lt.name
	
-- Anna mulle mitu laenu mingist 
-- tüübist on võetud ja mis on nende summa
SELECT 
	lt.name,
	COUNT(l.amount), -- INNER JOIN'i puhul võib sulgudesse * ka panna, 
	--sest nulle ei loe, aga left join'i puhul on oluline, et oleks l.amount, et nulle ei loendaks
	SUM(l.amount)
FROM
	loan AS l
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
GROUP BY 
	lt.name

--Mis aastal sündinud võtsid kõige suurema summa laenu
SELECT 
	date_part('year', s.birthday),
	SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY 
	date_part('year', s.birthday)
ORDER BY
	SUM(l.amount) DESC
LIMIT 1 -- mitu kirjet ma tahan

----Anna mulle õpilaste eesnime esitähe esinemise statistika
-- ehk mitme õpilase eesnimi algab mingi tähega
SELECT 
-- mis sõnast, mitmendast tähest ja mitu tähte
-- algab 1st
	SUBSTRING (first_name, 1,1),
	COUNT(first_name)
FROM
	student 
GROUP BY
 	SUBSTRING (first_name, 1,1)
ORDER BY
	SUBSTRING (first_name, 1,1)
	
-- leia sõnapaari esimene sõna
SELECT SUBSTRING('kiisu saba', 1, POSITION(' 'in 'kiisu saba')-1)

--leia sõnapaari teine sõna
SELECT SUBSTRING('kiisu saba', POSITION(' 'in 'kiisu saba')+1)

--Subquery or Inner query or Nested query

--Anna mulle õpilased kelle pikkus vastab keskmisele õpilaste pikkusele
SELECT
	first_name, last_name, height
FROM
	student
WHERE
	height = (SELECT ROUND(AVG(height)) FROM student)
	
--Anna mulle õpilased kelle eesnimi on keskmise pikkusega õpilaste 
--keskmine nimi ja kelle kaal on õpilase kaal, kelle id = 5
SELECT
	first_name, last_name, (SELECT weight FROM student WHERE id = 5)
FROM
	student
WHERE 
	first_name IN
(SELECT
	middle_name
FROM
	student
WHERE
	height = (SELECT ROUND(AVG(height)) FROM student))

--Lisa kaks vanimat õpilast töötaja tabelisse
INSERT INTO employee (first_name, last_name, birthday, middle_name)
SELECT first_name, last_name, birthday, middle_name FROM student ORDER BY birthday DESC LIMIT 2	

-- Anna mulle kõik matemaatikat õppivad õpilased
SELECT
	stu.first_name
FROM
 	student AS stu
JOIN
	student_subject AS ss
	ON stu.id = ss.student_id
JOIN
	subject AS sub
	ON sub.id = ss.subject_id
WHERE
	sub.name = 'Matemaatika'

-- Anna mulle kõik õppeained, kus kati õpib
SELECT
	sub.name
FROM
 	student AS stu
JOIN
	student_subject AS ss
	ON stu.id = ss.student_id
JOIN
	subject AS sub
	ON sub.id = ss.subject_id
WHERE
	stu.first_name = 'Kati'
	
-- Teisendamine täisarvuks
SELECT CAST(ROUND(AVG(age))AS UNSIGNED) FROM emp99
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	




	
	



