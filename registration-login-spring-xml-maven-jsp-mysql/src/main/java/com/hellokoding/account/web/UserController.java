package com.hellokoding.account.web;

import com.hellokoding.account.model.Business_trip;
import com.hellokoding.account.model.Role;

import com.hellokoding.account.model.User;
import com.hellokoding.account.repository.EmployeeDao;
import com.hellokoding.account.repository.RoleRepository;
import com.hellokoding.account.repository.TripDao;
import com.hellokoding.account.service.SecurityService;
import com.hellokoding.account.service.UserService;
import com.hellokoding.account.validator.UserValidator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;
    
    @Autowired
    private RoleRepository roleRepository;
    
    @Autowired  
    TripDao dao;//will inject dao from xml file  
    
    @Autowired  
    EmployeeDao employeeDao;//will inject dao from xml file 

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new User());
        model.addAttribute("roles", rolesToMap(roleRepository.findAll()));

        return "registration";
    }    

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String user(Model model) {
    	  List<Business_trip> list=dao.getBusiness_trips(employeeDao.getEmployeeByUserId(userService.getLoggedInUser().getId()).getId());  
	      model.addAttribute("list",list);    	
      //  model.addAttribute("userForm", new User());
        return "user";
    }
    
    @RequestMapping(value = {"/admin"}, method = RequestMethod.GET)
    public String admin(Model model) {
    	List<Business_trip> list=dao.getBusiness_trips();  
	      model.addAttribute("list",list);    	
        return "admin";
    }
    
    @RequestMapping(value = "/manager", method = RequestMethod.GET)
    public String manager(Model model) {
    	List<Business_trip> list=dao.getBusiness_trips();  
	      model.addAttribute("list",list);    	
      //  model.addAttribute("userForm", new User());
        return "manager";
    }
    
    @RequestMapping(value = "/head_manager", method = RequestMethod.GET)
    public String head_manager(Model model) {
      //  model.addAttribute("userForm", new User());
        return "head_manager";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
        userValidator.validate(userForm, bindingResult);
        
        if (bindingResult.hasErrors()) {
            return "registration";
        }

        userService.save(userForm);

        securityService.autologin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/employeeform";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model) {
    	if(userService.hasRole("ROLE_USER")) {
	    	return "redirect:/user";
    	}
    	else if(userService.hasRole("ROLE_ADMIN")) {
	    	return "redirect:/admin";
    	}
    	else if(userService.hasRole("ROLE_MANAGER")) {
	    	return "redirect:/manager";
    	}
    	else if(userService.hasRole("ROLE_HEAD_MANAGER")) {
	    	return "redirect:/head_manager";
    	}    	
    	
    	return "redirect:/viewemployee";
    	
//	    
//	    if(userService.hasRole("ROLE_MANAGER")) {
//	    	return "redirect:/manager";
//	    }
//        return "welcome";
    }
   
    
    
    private Map<Long, String> rolesToMap(List<Role> roles) {
    	Map<Long, String> rolesMap = new HashMap<>();
    	
    	for(Role role : roles) {
    		rolesMap.put(role.getId(), role.getName());
    	}
    	
    	return rolesMap;
    }

             
}
