<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

		<h1>Add New Employee</h1>
       <form:form method="post" action="save">  
      	<table >  
      	 <tr>  
          <td>First name :</td>  
          <td><form:input path="first_name" /></td>
<%--           <td><form:input id = "first_name" onchange = "calculatesalary()" path="first_name" /></td> --%>
         </tr>
         <tr>  
          <td>Last name :</td>  
          <td><form:input path="last_name" /></td>
         </tr>  
         <tr>  
          <td>Personal code :</td>  
          <td><form:input path="personal_code" /></td>
         </tr> 
         <tr>  
<!--           <tr>   -->
<!--           <td>Salary :</td>   -->
<%--           <td><form:input id = "salary" path="salary" /></td> --%>
<!--          </tr>  -->
<!--          <tr>   -->
          <td>Is active :</td>  
          <td><form:checkbox path="active" /></td>
         </tr> 
              <tr>  
          <td>User :</td>  
          <td><form:select path="user_id" items="${users}" /></td>
         </tr>

         <tr>  
          <td> </td>  
          <td><input type="submit" value="Save" />
          <button class="btn btn-lg btn-primary btn-block" type="button" onclick="window.location.href='viewemployee'">Back</button></td> 
<!--           <td><input id = "button" type="submit" value="Save" /></td>   -->
         </tr>  
        </table>  
       </form:form>  
<!--        <script> -->
<!--       	document.getElementById("button").value = "Salvesta" -->
<!--       	function calculateSalary() { -->
<!--       	if (document.geteElementById("first_name".value)== "Peeter") { -->
<!--       		document.getElementById('salary').value = 2000; -->
<!--       	} -->
<!--       	else { -->
<!--       		document.getElementById('salary').value = 1000; -->
<!--       	} -->
       
<!--        </script> -->
