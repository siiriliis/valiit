package it.vali;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    //Küsi kasutajalt kaks numbrit ja prindi nende summa string ->int

        Scanner scanner = new Scanner (System.in);

        System.out.println("Sisesta esimene number: ");
        int  number1 = Integer.parseInt(scanner.nextLine()); //nextLine tagastab alati stringi.
                            // Parse võtab sisu stringina ja teisendab selle integeriks ehk täisarvuks
        System.out.println("Sisesta teine number: ");
        int number2 = Integer.parseInt(scanner.nextLine());

        //System.out.println( "Arvude summa on: " + (number1 + number2));
        System.out.printf("Summa on: %d \n", number1 + number2);  // %d täisarvu korral

        //Küsi kaks reaalarvu ja prindi nende jagatis
        System.out.println("Sisesta esimene reaalarv: ");
        double number3 = Double.parseDouble(scanner.nextLine()); //kasutaja sisestab double aga nextLine loeb Stringina, seetõttu double.parseDouble teisendab selle double tüüpi
        System.out.println("Sisesta teine reaalarv: ");
        double number4 = Double.parseDouble(scanner.nextLine());
        System.out.printf("Jagatis on: %.1f \n", number3 / number4); //%f komaga arvu korral, .1 või .2 jne annab komakoha täpsuse

        // Kui tahan näha, 0,56 asemel 0.56, siis ütlen eraldi ette, et kasuta USA local-i
        System.out.println(String.format(Locale.US, "%.2f", number3/number4));
    }
}
