package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    // nulliga jagamise Exception
        // kasutda try/catch

        int a = 0;
        try {
            int b = 4 / a ;
            String word = null; //null => väärtus on tühi hulk
            word.length(); // tühjuselt pikkust küsida ei saa, peaks andma RuntimeExceptioni
            // kui esimene viga tekib, siis järgmisteni enam ei liigu ja neid kinni ei püüa. tuleks teha eraldi try catch kui ka tahaks, et järgmise vea leiaks


        }
        // kui on mitu Catch plokki, siis otsib ta esimese ploki,
        // mis oskab antud Exceptionit kinni püüda => Exception => RuntimeException => ArithmeticException
        catch (ArithmeticException e){ // lõppu tuleb e panna
            if (e.getMessage().equals("/ by zero")) {
                System.out.println("Nulliga ei saa jagada");
            }
            else {
                System.out.println("Esines aritmeetiline viga"); // prindib aint selle välja
                System.out.println(e.getMessage()); //saan Exceptioni käest küsida veateksti => / by zero
                // => hea kirjutada logifaili, mis viga mul esines
                // (kasutajale ei taha seda näidata)
            }
        }
        catch (RuntimeException e) { // lõppu tuleb e panna
            System.out.println("Esines reaalajaas esinev viga");
        }

        // Exception on klass, millest kõik erinevad Exceptioni tüübid pärinevad,
        // mis omakorda tähenda, et püüdes kinni üldise Exceptioni,
        // püüame me kinni kõik Exceptionid
        catch (Exception e) { // selle paneb alati lõppu, et püüaks ka ülejäänud kinni
            //e.printStackTrace();
            System.out.println("Esines viga");
        }

        // Küsime kasutajalt numbri ja kui number ei ole õiges formaadis (ei ole number), siis ütleme veateate
        // teha tsükkel
        Scanner scanner = new Scanner(System.in);
            //variant  kui kasutada breaki
//        do {
//            System.out.println("Sisesta number");
//            try {
//                int input = Integer.parseInt(scanner.nextLine());
//                break;
//            } catch (NumberFormatException e) {
//                //e.printStackTrace();
//                System.out.println("Esines viga.");
//            }
//        } while (true);

        //variant 1 booleani kasutamiseks kui incorrectNumber = false

//        boolean incorrectNumber;
//        do {
//            incorrectNumber = false;
//            System.out.println("Sisesta number");
//            try {
//                int input = Integer.parseInt(scanner.nextLine());
//
//            } catch (NumberFormatException e) {
//                //e.printStackTrace();
//                System.out.println("Esines viga.");
//                incorrectNumber = true;
//            }
//        } while (incorrectNumber);

        //variant 2 booleani kasutamiseks kui correctNumber = false

        boolean correctNumber = false;
        do {
            System.out.println("Sisesta number");
            try {
                int input = Integer.parseInt(scanner.nextLine());
                correctNumber = true; // läheb true-ks ja tsükkel saab läbi, sest corretNumber ei ole võrdne false

            } catch (NumberFormatException e) {
                //e.printStackTrace();
                System.out.println("Esines viga.");
            }
        } while (!correctNumber);

        // Loo täisarvude massiv 5 täisarvuga ning ürita sinna lisada kuues täisarv
        // Näita veateadet

        int[] numbers = new int [] {1,2,3,4,5};
        try {
            numbers [5] = 6;
        }
        catch (ArrayIndexOutOfBoundsException e) {
           System.out.println("Indeks, kuhu tahtsid väärtust määrata, ei eksisteeri");
        }
        //System.out.println(); // lühend sout +enter

    }
}
