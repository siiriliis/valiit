package it.vali;

public class Main {

    public static void main(String[] args) {
	    // Casting on teisendamine ühest arvutüübist teise
        // väiksemaks suuremaks ei pea castima, suuremast väiksemasse peab
        byte a = 57;
        short b = a; //Kui üks numbri tüüp mahub teise sisse,
                    // siis toimub automaatne teisendamine (implicit casting)

        System.out.println(b); // vastus 57

        byte c = (byte) b; // kui üks numbritüüp ei pruugi mahtuda teise sisse,
                            // siis peab kõigepealt ise veenduma, et see number mahu teise
                            //  numbritüüpi ja kui mahub,// siis peab ise seda teisendama
                            // (explicit casting)
        System.out.println(c); // vastus ikka 57

        short d = 300;
        byte e = (byte) d; //c -sharpis annab vea, siis hakkab jälle ringiratast minema
        System.out.println(e); // vastus 44, sest hakkab ringiratast käima 300 - 256 = 44

        long n = 100000000000L;
        int m = (int) n; // cast to int
        System.out.println(m); //c -sharpis annab vea, siis hakkab jälle ringiratast minema,
                                // vastus 1215752192

        float h = 123.2324354F;
        double i = h;

        float j = (float) i;

        double k = 55.11111111111111111111111;
        float l = (float)k; // antud juhul toimub ümardamine
        System.out.println(l); // vastus 55.11111

        double f = 12E40; // 12 *10 astmes 40, mis ei mahu float sisse, aga java ei anna selle kohta errorit vaid käib ringiratast
        float g= (float) f;
        System.out.println(g); // vastuseks tuli infinity

        double p = 2.99;
        short q = (short) p; // võtab komakohad ära
        System.out.println(q); //vastus on 2

        // int jagatud int on vastus ka int tüüpi
        // int jagatud double on vastus double tüüpi
        int r = 2;
        double s = 9;
        System.out.println(r/s); // vastus on double tüüpi

        int u = 4;
        int v = 15;
        // int / int = int
        // int / double = double
        //double / int = double
        //double + int = double
        // double / float = double
        System.out.println((double)u/v); //vastus on double tüüpi
        System.out.println((float)u/v);

        float tu= 12.5555555F;
        double t = 23.555555;
        System.out.println(tu/t); //vastus on double tüüpi

    }
}
