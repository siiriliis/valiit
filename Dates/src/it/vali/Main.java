package it.vali;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
	// write your code here
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss EEEE"); // E annab esimese tähe, EEEE annab terve päeva
        Calendar calendar = Calendar.getInstance();
        //Move calendar to yesterday
        calendar.add(Calendar.DATE, -1); // võta eelmine päev
        Date date = calendar.getTime(); // tekita mulle kuupäev kalendrist
        System.out.println(dateFormat.format(date));

//        calendar.add(Calendar.YEAR, 1); // liigutab järgmisesse aastatsse
//        date = calendar.getTime();
//        System.out.println(dateFormat.format(date));

        // Prindi ekraanile selle aasta järele jäänud kuude esimese kuupäeva nädalapäev

        System.out.println(calendar.get(Calendar.MONTH)+1);
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1);
        date = calendar.getTime();
        System.out.println(dateFormat.format(date));

        DateFormat weekdayFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss EEEE");

//        for (int i = calendar.get(Calendar.MONTH) ; i < 11  ; i++) {
//            calendar.add(Calendar.MONTH, 1);
//            date = calendar.getTime();
//            System.out.println(weekdayFormat.format(date));
//        }
        int currentYear = calendar.get(Calendar.YEAR);
        calendar.add(Calendar.MONTH, 1);
        while (calendar.get(Calendar.YEAR) == currentYear) {

            date = calendar.getTime();
            System.out.println(weekdayFormat.format(date));
            calendar.add(Calendar.MONTH, 1);
        }
    }
}
