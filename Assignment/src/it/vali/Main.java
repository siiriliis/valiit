package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    // Ülesanne 1: Deklareeri muutuja, mis hoiaks endas Pi arvulist väärtust vähemalt 11 kohta peale koma.
        // Korruta selle muutuja väärtus kahega ja prindi standardväljundisse

        double pi = Math.PI;
        System.out.printf("%.11f%n", pi*2);

        //Ül2:

        System.out.println(equal(3,4));
        System.out.println();

        // Ül 3:
        String [] words = new String [] {"seitse", "kaks"};
        int [] numbers = stringArrayToIntArray(words);
        for (int i = 0; i < numbers.length; i++) {
            System.out.printf("Sõnas on %d tähte %n", numbers[i]);
        }

        // ül 4:
        System.out.println(number(0));
        System.out.println(number(1));
        System.out.println(number(128));
        System.out.println(number(598));
        System.out.println(number(1624));
        System.out.println(number(1827));
        System.out.println(number(1996));
        System.out.println(number(2017));
        System.out.println();

        //Ül 5
        Country estonia = new Country();
        estonia.setName("Eesti");
        estonia.setPopulation(1300000);
        List<String>languages = new ArrayList<>();
        languages.add("estonian");
        languages.add("russian");
        estonia.setLanguages(languages);

        System.out.println(estonia);

    }
    //Ülesanne 2:
    static boolean equal (int x, int y) {
        if (x ==y) {
            return true;
        }
        return false;
    }

    //Ülesanne 3:
    static int [] stringArrayToIntArray (String [] words) {
        int [] numbers = new int [words.length];
        //words [0] = kaks;  numbers [0] =4
        // words [1] = üks;  numbers [1] =3
        for (int i = 0; i <words.length ; i++) {
            numbers[i] = generateANumber((words[i]));

        }

        return numbers;
    }
    static int generateANumber (String word) {
        int number = 0;

        for (int i = 0; i <word.length() ; i++) {
            number++;

        }
        return number;
    }

    // ülesanne 4:

     static byte number (int year) {
            if (year < 1 || year > 2018) {
                return -1;
            }
            int century = year/100 +1;
            byte yearbyte = (byte) century;
            return yearbyte;
     }

}