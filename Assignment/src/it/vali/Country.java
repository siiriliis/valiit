package it.vali;

import java.util.List;

public class Country {
    private double population;
    private String name;
    private List<String> languages;


    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public double getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }
//    public void printInfo() {
//        System.out.println("Info");
//        System.out.printf("Riigi nimi %s%n",getName());
//        System.out.printf("Rahvaarv %d%n", getPopulation());
//        System.out.printf("Keel %s%n", languages);

    @Override
    public String toString() {
        System.out.println("Info:");
        //return name +" " + population +" "+ languages;
        return String.format("Country: %s%nPopulation: %.1f million%nLanguages: %s%n", name, population, String.join(", ", languages));

    }
}
