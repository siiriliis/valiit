package it.vali;

public class Main {

    public static void main(String[] args) {
	    // Inheritance = põlvnemine

        // teeme kassi
        Cat angora = new Cat();

        // Lisame kassi parameetrid
        angora.setAge(10);
        angora.setBreed("Angora");
        angora.setName("Miisu");
        angora.setWeight(4.4);

        //prindi kassi info
        angora.printInfo();

        // prindi kass sööb
        angora.eat();

        System.out.println();

        // teeme veel ühe kassi
        Cat persian = new Cat();

        // Lisame kassi parameetrid
        persian.setAge(6);
        persian.setBreed("Persian");
        persian.setName("Pätu");
        persian.setWeight(3.4);

        //prindi kassi info
        persian.printInfo();

        // prindi kass sööb
        persian.eat();

        System.out.println();

        // tee koer
        Dog tax = new Dog();

        // Lisame koera parameetrid
        tax.setAge(7);
        tax.setBreed("Tax");
        tax.setName("Saba");
        tax.setWeight(2.5);

        //prindi koera info
        tax.printInfo();

        // prindi koer sööb
        tax.eat();

        persian.catchMouse();
        tax.playWithCat(persian);

        // Lisa pärinevusahelast puuduvad klassid
        // igale klassile lisa 1 parameeter/muutuja
        // ja 1 meetod, mis on sellele klassile omane

        Cow cow = new Cow();
        cow.setName("Mumm");
        cow.name();



    }
}
