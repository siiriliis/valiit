package it.vali;

public class Pet extends DomesticAnimal {

    private boolean hasHome = true;


    public boolean isHasHome() {
        return hasHome;
    }

    public void setHasHome(boolean hasHome) {
        this.hasHome = hasHome;
    }

    public void home () {
        if (hasHome) {
            System.out.println("Mul on tore kodu");
        }
        else {
            System.out.println("Otsin kodu");
        }
    }



}
