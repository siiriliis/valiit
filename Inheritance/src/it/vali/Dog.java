package it.vali;

public class Dog extends Pet {
//    private String name;
//    private String breed; // tõug
//    private int age;
//    private double weight;

    private boolean hasTail = true; // vaikimisi on false

    public boolean isHasTail() {
        return hasTail;
    }

    public void setHasTail(boolean hasTail) {
        this.hasTail = hasTail;
    }


//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getBreed() {
//        return breed;
//    }
//
//    public void setBreed(String breed) {
//        this.breed = breed;
//    }
//
//    public int getAge() {
//        return age;
//    }
//
//    public void setAge(int age) {
//        this.age = age;
//    }
//
//    public double getWeight() {
//        return weight;
//    }
//
//    public void setWeight(double weight) {
//        this.weight = weight;
//    }
//
//    // meetod "Söön"
//    public void eat() {
//        System.out.printf("Mina nimi on %s. Söön, nam, nam %n", name);
//    }
//    //prindi koera parameetreid
//    public void printInfo() {
//        System.out.println("Koera info:");
//        System.out.printf("Nimi: %s%n", name);
//        System.out.printf("Tõug: %s%n", breed);
//        System.out.printf("Vanus: %d%n", age);
//        System.out.printf("Kaal: %.1f%n%n", weight);
//    }

    // meetod mängin kassiga
    public void playWithCat (Cat cat) { // klass kass Cat ( nagu String, int, boolean)
        System.out.printf("Mängin kassiga %s%n", cat.getName());

    }


}
