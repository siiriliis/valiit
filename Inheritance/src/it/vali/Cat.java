package it.vali;

public class Cat extends Pet{ // kasutades extendi, siis ühiseid omadused on vaja kirjutada vaid animals klassi
//    private String name;
//    private String breed; // tõug
//    private int age;
//    private double weight;

    private boolean hasFur = true; // vaikimisi false

    // get ja set on vajalikud selleks, et main-is saaks kirjutada
    // Cat angora = new Cat();
    // angora.age(10), => nii ei saa, sest see on private
    // aga nüüd saab panna angora.setAge(10)

    public boolean isHasFur() {
        return hasFur;
    }

    public void setHasFur(boolean hasFur) {
        this.hasFur = hasFur;
    }

//    // meetod "Söön"
//    public void eat() {
//        System.out.printf("Minu nimi on %s. Söön, nam, nam %n", name);
//    }
//    //prindi kassi parameetreid
//    public void printInfo() {
//        System.out.println("Kassi info:");
//        System.out.printf("Nimi: %s%n", name);
//        System.out.printf("Tõug: %s%n", breed);
//        System.out.printf("Vanus: %d%n", age);
//        System.out.printf("Kaal: %.1f%n%n", weight);
//    }

    // meetod kass püüab hiiri
    public void catchMouse () {
        System.out.println("Püüdsin hiire kinni");
    }


}
