package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);

        System.out.println("Sisesta number: ");
        int a = Integer.parseInt(scanner.nextLine());

      //Java on type-safe language, st igal muutujal on tüüp, kui a on int, siis ma ei saa muuta teda stringiks.

            if (a == 3) { // kui if sees on ainult üks tegevus, töötab ka ilma loogsulgudeta
                System.out.printf("Arv %d on võrdne kolmega %n", a);
            }
            if (a < 5) {
                System.out.printf("Arv %d on väiksem viiest %n", a);
            }
            if (a != 4) {
                System.out.printf("Arv %d ei ole võrdne neljaga %n", a);
            }
            if (a > 2) {
                System.out.printf("Arv %d on suurem kahest %n", a);
            }
            if (a >= 7) {
                System.out.printf("Arv %d on suurem või võrdne seitsmega %n", a);
            }
            if (!(a >=9)) {
                System.out.printf("Arv %d ei ole suurem või võrdne üheksaga %n", a);
            }
            // kui tingimuste vahel on jaa (&&), kui leitakse, et esimene on vale, siis edassi tingimusi ei vaadata
            if (a > 2 && a < 8 ) {
                System.out.printf("Arv %d on suurem kui 2 ja väiksem kui 8 %n", a);
            }
            if (a <2 || a >8) {
                System.out.printf("Arv %d on väiksem kui 2 või suurem kui 8 %n", a);
            }
            if ( (a > 2 && a < 8) || (a > 5 && a < 8) || (a > 10)) { // jaatus on prioriteetsem kui või ehk siin töötaks ka ilma sulgudeta, vastupidi ei töötaks
                System.out.printf("Arv %d on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10 %n", a);
            }
            if (!(a > 4 && a < 6) && (a > 5 && a < 8)) {
                System.out.printf("Arv %d ei ole 4 ja 6 vahel aga on 5 ja 8 vahel %n", a);
            }
            if ((!(a > 4 && a < 6) && (a > 5 && a < 8) )|| (a < 0) && !(a > -14)) {
                System.out.printf("Arv %d ei ole 4 ja 6 vahel aga on 5 ja 8 vahel või arv on negatiivne aga pole suurem kui -14 %n", a);
            }
    }
}
