package it.vali;

import java.io.*;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Scanner;

public class Main {
    static String pin;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int balance = loadBalance();

        //System.out.println(currentDateTimeToString() + " Sularaha väljavõtt -100");
        //Lisa konto funktsionaalsus
        //DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss"); //määran formaadi
        //Calendar cal = Calendar.getInstance(); // kutsun välja kalendri
        //System.out.println(dateFormat.format(cal));



        pin = loadPin();

        boolean pinCorrect = true;
        for (int i = 0; i <3 ; i++) {
            System.out.println("Sisesta PIN:");
            String InputPin = scanner.nextLine();
            if (InputPin.equals(pin)) {
                System.out.println("Pin õige \n");
                break;
            }
            else {
                System.out.println("PIN on vale");
                pinCorrect = false;
            }
        }
        if (pinCorrect !=true) {
            System.out.println("Sisestaisd PIN koodi 3 korda valesti.");
           return;
        }


        do {
            boolean exit;
            do {
                exit = false;
                System.out.println("Vali toiming: " +
                        "a)sularaha sissemakse,\n" +
                        "b)sularaha väljamakse,\n" +
                        "c)kontojääk, \n" +
                        "d)muuda pin kood)");

                String input = scanner.nextLine();
                input = input.toLowerCase();
                if (input.equals("a")) {
                    System.out.println("Sisesta raha");
                    int amount = Integer.parseInt(scanner.nextLine());
                    balance = loadBalance();
                    balance += amount;
                    saveBalance(balance);
                    System.out.printf("Sinu kontole lisati %d ja jääk on %d %n",amount, balance);
                    break;
                }
                else if (input.equals("b")) {
                    System.out.println("Vali summa: " +
                            "a) 5 eur " +
                            "b) 10 eur " +
                            "c) 20 eur " +
                            "d) Muu summa");
                    int amount = 0;

                    int number = 0;
                    String answer = scanner.nextLine();
                    answer.toLowerCase();
                    exit =false;
                    do {
                        balance = loadBalance();
                        if (answer.equals("a")){
                            amount = 5;
                            if (amount < balance) {
                                balance -= amount;
                                saveBalance(balance);
                                System.out.printf("Sinu kontolt võeti %d ja jääk on %d %n",amount, balance);
                            }
                            else {
                                System.out.println("Kontol pole piisavalt raha");
                            }

                        }
                        else if (answer.toLowerCase().equals("b")){
                            amount = 10;
                            if (amount > balance) {
                                System.out.println("Kontol pole piisavalt raha");
                            }
                            else {
                                balance -= amount;
                                saveBalance(balance);
                                System.out.printf("Sinu kontolt võeti %d ja jääk on %d %n",amount, balance);

                            }

                        }
                        else if (answer.toLowerCase().equals("c")){
                            amount = 20;
                            if (amount > balance) {
                                System.out.println("Kontol pole piisavalt raha");
                            }
                            else {
                                balance -= amount;
                                saveBalance(balance);
                                System.out.printf("Sinu kontolt võeti %d ja jääk on %d %n",amount, balance);

                            }

                        }
                        else if (answer.toLowerCase().equals("d")){
                            System.out.println("Sisesta summa");
                            number = Integer.parseInt(scanner.nextLine());
                            amount = number;
                            if (amount > balance) {
                                System.out.println("Kontol pole piisavalt raha");

                            }
                            else {
                                balance -= amount;
                                saveBalance(balance);
                                System.out.printf("Sinu kontolt võeti %d ja jääk on %d %n",amount, balance);

                            }
                        }

                    } while (exit);



                }
                else if (input.equals("c")) {
                    balance = loadBalance();
                    System.out.printf("Sinu kontojääk on %d%n", balance);
                    break;
                } else if (input.equals("d")) {

                    pinCorrect = true;
                    for (int i = 0; i < 3; i++) {
                        System.out.println("Sisesta vana pin");
                        input = scanner.nextLine();
                        if (input.equals(pin)) {
                            System.out.println("Vali uus pin");
                            String newPin = scanner.nextLine();
                            savePin(newPin);
                            System.out.println("Pin edukalt muudetud");
                            break;
                        }
                        else {
                            System.out.println("Pin on vale");
                            pinCorrect = false;
                        }
                    }
                    if (pinCorrect != true) {
                        System.out.println("Sisestasid Pin koodi 3 korda valesti");
                    }
                }
                else {
                    System.out.println("Selline valik puudub");
                    exit = true;
                }

            } while (exit);
            System.out.println("Kas sa tahad väljuda? jah/ei");
        } while (scanner.nextLine().toLowerCase().equals("ei"));
        System.out.println("Ilusat päeva!");

    }

    // Toimub pin  kirjutamine pin.txt faili
    static void savePin(String pin) {
        try {
            FileWriter fileWriter = new FileWriter("pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Pin koodi salvestamine ebaõnnestus.");
        }
    }

    // Toimub pin välja lugemine pin.txt failist
    static String loadPin() {
        // toimub shadowing
        //Shadowing refers to the practice in Java programming of using
        // two variables with the same name within scopes that overlap.
        // When you do that, the variable with the higher-level scope
        // is hidden because the variable with lower-level scope overrides it.
        // The higher-level variable is then “shadowed.
        String pin = null;
        try {
            FileReader fileReader = new FileReader("pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();

        } catch (IOException e) {
            System.out.println("Pin koodi lugemine ebaõnnestus");
        }
        return pin;
    }

    static boolean validatePin () {
        Scanner scanner = new Scanner(System.in);
        pin = loadPin();
        for (int i = 0; i <3 ; i++) {
            System.out.println("Palun sisesta PIN kood");
            String enteredPin = scanner.nextLine();

            if (enteredPin.equals(pin)) {
                System.out.println("PIN on õige");
                return true;

            }
            else {
                System.out.println("Sisestatud PIN on vale");
            }
        }
        return true;
    }



    // Toimub balance kirjutamine balance.txt faili
    static void saveBalance(int balance) {

        try {
            FileWriter fileWriter = new FileWriter("balance.txt");
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus.");
        }
    }

    // Toimub balance välja lugemine balance.txt failist
    static int loadBalance() {

        int balance = 0;
        try {
            FileReader fileReader = new FileReader("balance.txt", Charset.forName("Cp1252"));
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = Integer.parseInt(bufferedReader.readLine());
            bufferedReader.close();
            fileReader.close();

        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");
        }
        return balance;
    }


    static String currentDateTimeToString() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss"); //määran formaadi
        //Get current date of calendat which point to the yesterday now
        Date date = new Date();
       // Calendar cal = Calendar.getInstance(); // kutsun välja kalendri
       // System.out.println(dateFormat.format(cal));
        return  dateFormat.format(date);
    }
//    static boolean validatePin() {
//        Scanner scanner = new Scanner(System.in);
//        for (int i = 0; i < 3; i++) {
//            System.out.println("Palun sisesta PIN kood");
//            String enteredPin = scanner.nextLine();
//
//            if (enteredPin.equals(pin)) {
//                System.out.println("Õige");
//                // break; // hüppab for tsüklist välja
//                return true; // hüppab ka for tsüklist välja
//            }
//
//        }
//        return false;
//
//
//    }



}
//Saab ka teha While meetodiga
//        int counter = 3;
//        while (counter != 0) {
//            System.out.println("Sisesta PIN:");
//            String InputPin = scanner.nextLine();
//            if (InputPin.equals(pin)) {
//                System.out.println("Pin õige \n");
//                break;
//            }
//            else {
//                System.out.println("PIN on vale");
//                counter--;
//            }
//            if (counter == 0) {
//                System.out.println("Rohkem proovida ei saa");
//
//            }
//        }
