package it.vali;

public class Main {

    public static void main(String[] args) {
	// write your code here
        //Method Overriding ehk meetodi üle kirjutamine
        // päritava klassi meetodi sisu kirjutatakse pärinevas klassis üle
        Dog tax = new Dog();
        tax.eat();

        Cat siam = new Cat();
        siam.eat();

        Pet pet = new  Pet();
        siam.printInfo();

        System.out.printf("Koera vanus on: ");
        System.out.println(tax.getAge());
        System.out.printf("Kassi vanus on: ");
        System.out.println(siam.getAge());

        //kirjuta koera  getAge üle nii, et kui koeral vanus on 0, siis näitaks 1

        // metsloomadel printinfo võiks kirjutada Nimi: metsloomal pole nime
        Wolf wolf = new Wolf();
        wolf.setName("kutsu"); // ei prindi nime, sest oleme overridinud nime
        wolf.printInfo();
        Bear bear = new Bear();
        bear.printInfo();


    }
}
