package it.vali;

public class Animal extends Object{


    private String name;

    private String breed; // tõug
    private int age;
    private double weight;
    //protected double weight;


    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {

        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
    public String getName() {
        if (name == null) {
            return "puudub";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // meetod "Söön"
    //tee nii, et koer saab öelda, et söön konti ja kass sööb hiiri
    public void eat() {
        System.out.printf("Söön, nam, nam %n");
    }
    //prindi looma parameetreid
    public void printInfo() {
        System.out.println("Looma info:");
        System.out.printf("Nimi: %s%n",getName());
        System.out.printf("Tõug: %s%n", breed);
        System.out.printf("Vanus: %d%n", age);
        System.out.printf("Kaal: %.1f%n", weight);


    }

    // animal => WildAnimal, DomesticAnimal
    // WildAnimal => Herbivoreanimal, CarnivoreAnimal
    //DomesticAnimal => Pet, FarmAnimal
    // Pet => kass, koer
    // implements
    //igasse ühte kaks loom
}
