package it.vali;

public class FarmAnimal extends DomesticAnimal {
    private String farmName;
    public String getFarmName() {
        return farmName;
    }

    public void setFarmName(String farmName) {
        this.farmName = farmName;
    }

    public void farm () {
        System.out.printf("Elan talus nimega %s%n", farmName);
    }
}
