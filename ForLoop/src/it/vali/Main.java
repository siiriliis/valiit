package it.vali;

public class Main {

    public static void main(String[] args) {
	    // For tsükkel on selline tsükkel, kus  korduste arv on teada
        // lõpmatu for tsükkel
//        for( ; ; ) {
//            System.out.println("Väljas on ilus ilm");
//        }


        // 1) int i = 0 => saab luua muutujaid ja neid alfväärtustada.
        // Luuakse täisarv i, mille väärtus hakkab tsükli sees muutuma
        // 2) i < 3 => tingimus, mis peab olema tõene, et tsükkel käivituks ja korduks
        // 3) i++ => see on tegevus, mida iga tüskli korduse lõppus korratakse
        // i++ on sama, mis i = i + 1
        // i-- on sama, mis i = i - 1
        for (int i = 0; i < 3; i++) { //fori + enter on shortcut, siis ei pea ise sulge tegema ja sisu
            System.out.println("Väljas on ilus ilm");
        }

        // Prindi ekraanile numbrid 1 kuni 10
        //variant1 => ei ole väga ilus
        for (int i = 0; i <10; i++) {
            System.out.println(i + 1);
        }
        System.out.println(); // tühi rida
        //variant2
        for (int i = 1; i <=10; i++) {
            System.out.println(i);
        }
        System.out.println();
        //variant3 => ei ole väga ilus
        for (int i = 1; i <11; i++) {
            System.out.println(i);
        }
        System.out.println();
        //Prindi ekraanile numbrid 24st 167ni
        //Prindi ekraanile numbrid 18st 3ni

        for (int i = 24; i <= 167 ; i++) {
            System.out.println(i);
        }
        System.out.println();
        for (int i = 18; i >= 3 ; i--) {
            System.out.println(i);
        }
        System.out.println();
        //Prindi ekraanile numbrid 2, 4 , 6, 8, 10
        for (int i = 2; i <= 10 ; i = i + 2) { // i = i+2 lühend on i+=2
            System.out.println(i);
        }
        System.out.println();
        //Prindi ekraanile numbrid 10 kuni 20 ja 40 kuni 60
        //variant 1 => ei ole kõige parem
              for (int i = 10; i <=20 ; i++) {
                System.out.println(i);
                if (i == 20) {
                    for (int j = 40; j <=60 ; j++) {
                        System.out.println(j);
                    }
                }
            }
            System.out.println();

        //variant 2
        for (int i = 10; i <= 60 ; i++) {
            if (i <=20 || i >= 40) {
                System.out.println(i);
            }
        }
        System.out.println();
        // variant 3
        for (int i = 10; i <=60 ; i++) {
            if (i==21) {
                i = 40;
            }
            System.out.println(i);
        }
        System.out.println();

        // Prindi kõik arvud, mis jaguvad 3ga vahemikus 10-50
        // a % 3

        for (int i = 10; i <=50 ; i++) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }
        // Leia kõik paaritud arvud
        for (int i = 10; i <=50 ; i++) {
            if (i % 2 != 0) {
                System.out.println(i);
            }
        }
    }
}
