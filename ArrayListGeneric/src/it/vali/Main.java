package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    // Generic arraylist on selline kollektsioon,
        // kus objekti loomise hetkel peame määrama,
        // mis tüüpi elemente see sisaldama hakkab

        List list = new  ArrayList(); // tavaline arraylist
        List <Integer> numbers = new ArrayList<Integer>(); // kõik elemndid on Integerid

        numbers.add(10);
        numbers.add(2);
        numbers.add(Integer.valueOf("10"));
        //numbers.add(Integer.valueOf("a")); => sedasi teha ei saa
        System.out.println(numbers);

        List <String> words = new ArrayList<String>();
        words.add("Tere");
        words.add("Hommikust");

    }
}
