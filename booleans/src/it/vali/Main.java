package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // booleaniga tähistatakse "true" või "false"
        boolean isMinor = false; // ma ei pea siin seda ära märkima kas on true või false = true;     //c-sharp on bool
        // boolean isGrownup = false; //saan mõlemat pidi kirjutada

        Scanner scanner = new Scanner(System.in);

        System.out.println("Kui vana sa oled?");
        int age = Integer.parseInt(scanner.nextLine());

        if (age < 18) {
            isMinor = true;
        }
        //else {
        //isMinor = false; //juhul kui ei ole märgitus üles boolean isMinor =false, vaid on ilma "=false" osata
        //}
        if (isMinor == true) { // ==true võib jätta kirjutamata
            System.out.println("Oled alaealine.");
        } else {
            System.out.println("Oled täisealine.");
        }

        int number = 4;
        boolean numberIsGreaterThan3 = number > 3;

        if (number > 2 && number < 6 || number > 10 || number < 20 || number == 100) {

        }
        boolean a = number > 2;
        boolean b = number < 6;
        boolean c = number > 10;
        boolean d = number < 20;
        boolean e = number == 100;
        boolean f = a && b;

        if (f || c || d || e) {

        }

        //Küsi kasutajalt kas ta on söönud hommikusööki
        // -||- lõunat
        // -||- õhtusööki
        // prindi ekraanile, kas kasutaja on täna söönud või mitte

        boolean hasEaten = false; //kuna kontrollolukord on true, siis siia ei tohi true panna

        System.out.println("Kas sa hommikusööki sõid? jah/ei");
        String answer = scanner.nextLine();

        if (answer.equals("jah")) {
            hasEaten = true;
            System.out.println("Kasutaja on söönud hommikust");

        }

        System.out.println("Kas sa lõunat sõid? jah/ei");
        answer = scanner.nextLine(); // siia ei ole vaja stringi ette, sest muutuja on juba defineeritud
        if (answer.equals("jah")) {
            hasEaten = true;
            System.out.println("Kasutaja on söönud lõunat");
        }

        System.out.println("Kas sa õhtusööki sõid? jah/ei");
        answer = scanner.nextLine();
        if (answer.equals("jah")) {
            hasEaten = true;
            System.out.println("Kasutaja on söönud õhtust");
        }

        if (hasEaten) {
            System.out.println("Kasutaja on söönud täna");
        }


    }

}



