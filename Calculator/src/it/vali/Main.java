package it.vali;

public class Main {

    public static void main(String[] args) {

        int summa =  calculateSum(4,5); // summa, calculateSum, sum => kõik võib sum ka kirjutada, endale arusaadavuse jaoks panin erinevad
        System.out.printf("Arvude 4 ja 5 summa on %d%n", summa);
        System.out.printf("Arvude 9 ja 5 vahe on %d%n", subract(9,5));
        System.out.printf("Arvude 9 ja 5 korrutis on %d%n", multiply(9,5));
        System.out.printf("Arvude 9 ja 5 jagatis on %.1f%n", divide(9,5));

        //        int[] numbers = new int[3]; => variant 1 massiivi loomiseks
//        numbers[0] = 1;
//        numbers[1] = 4;
//        numbers[2] = 6;
//        sum = sum(numbers);

        int [] numbers = new int[] {2,5,6,13}; // => variant 2 massiivi loomiseks
        int sum = sum (numbers);

        sum = sum(new int [] {2,3,6,4,6});  // => variant 3 massiivi loomiseks

        System.out.printf("massiivi arvude summa on %d%n", sum);

        int[] reversed = numbersReverse(numbers); // numbers massiiv on eelnevalt tehtud juba
        printNumbers(reversed);
        printNumbers(numbersReverse(new int[] {1,2,3,4,5})); // saab ka otse sisse panna numbrid

        printNumbers(convertToIntArray(new String[] {"2", "3", "5", "-8"}));

//        String[] numbersAsText = new String[] { "200", "-12", "1", "0", "17" };
//        int[] numbersAsInt = convertToIntArray(numbersAsText);
//        printNumbers(numbersAsInt);

        String sentence = stringArrayToSentence(new String[] {"Miks", "ma",  "sellest",  "midagi",  "aru", "ei" ,"saa"});
        System.out.println(sentence);

        sentence = stringArrayToSentence(new String[] {"Miks", "ma",  "sellest",  "midagi",  "aru", "ei" ,"saa"}, " ei ");
        System.out.println(sentence);

        numbers = new int[] {2,3,10};
        double average = numbersAverage(numbers, numbers.length);
        System.out.println(average);

        //vaata kuidas Lauri tegi

        int a = 23;
        int b = 40;
        System.out.printf("Arv %d moodustab arvust %d %.2f%%  %n",
                a,b, percent(a,b)); // protsendi märgi jaoks %% (kaks % märki)

        double radius = 10.25;
        System.out.printf("Ringi raadiusega %.2f ümbermõõt on %.2f%n",
                radius, circumference(radius));

    }

    // Teeme summa leidmise meetodi, tagastab summa
    // Meetod, mis liidab 2 täisarvu kokku ja tagastab nende summa
    static int calculateSum (int a, int b) {
        int sum = a + b; // selle võib kirjutamata jätta
        return sum;
    }

    // subract
    static int subract (int a, int b) {
        return a -b ;
    }

    // multiply
    static int multiply (int a, int b) {
        return a * b;
    }
    // divide

    static double divide (int a, int b) {
        return(double) a / b;// üks täisarv peaks olema tehtud doubleks
    }
    // meetod, mis võtab parameetriks täisarvude massiivi ja liidab elemendid kokku ja tagastab summa
    static int sum (int[] numbers){

    int sum = 0;
        for (int i = 0; i <numbers.length ; i++) {
            sum += numbers[i];
        }
        return sum;
    }
    // Meetod, mis võtab parameetriks täisarvude massiivi, pöörab tagurpidi ja tagastab selle

    static int [] numbersReverse (int[] numbers){
        int [] numbersReverse = new int [numbers.length]; // loome uue massiivi
        for (int i = numbers.length-1, j = 0; i >= 0 ; i--, j++) {
            numbersReverse [j] = numbers[i];

        }
        return numbersReverse;
    }

    //Meetod, mis prindib välja täisarvude massiivi elemendid
    static void printNumbers (int[] numbers) {
        for (int i = 0; i <numbers.length ; i++) {
            System.out.println(numbers[i]);
        }
    }

    // Meetod, mis võtab parameetriks stringi massiivi
    // (eeldusel, et tegelikult seal massiivis on numbrid stringidena)
    // ja teisendab numbrite massiiviks ja tagastab selle
    static int[] convertToIntArray(String[] numbersAsText) {
        int[] numbers = new int[numbersAsText.length];
        for (int i = 0; i <numbers.length ; i++) {
            numbers[i] = Integer.parseInt(numbersAsText[i]);
        }

        return numbers;
    }

    // Meetod, mis võtab parameetriks stringi massiivi ning tagastab lause,
    // kus iga stringi vahel on tühik

    static String stringArrayToSentence (String [] stringMassive) {
        String sentence = String.join(" ", stringMassive);
        //return  String.join(" ", stringMassive);
        return sentence;
    }

    // Meetod, mis võtab parameetriks stringi massiivi ning teine parameeter on sõnade eraldaja
    // Tagastada lause.
    // Vaata eelmiset ülesannet, lihtsalt tühiku asemel saad ise valida, mille pealt sõnu eraldab

    static String stringArrayToSentence (String [] stringMassive, String delimiter) {
        String sentence = String.join(delimiter, stringMassive);
        return sentence;
    }

    // Meetod, mis leiab numbrite massiivist keskmise ning tagastab selle

    static double numbersAverage (int [] numbers, int howManyNumbers ) {
        //return sum(numbers) /(double)numbers.length; // siis pole ka int howManyNumbers parameetrit vaja
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];

        }
        int average = sum/howManyNumbers;
        return average;
    }

    // Meetod, mis arvutab mitu % moodustab esimene arv teisest.
    static double percent (int a, int b) {
        return a/(double)b *100;
    }

    // Meetod, ringi ümbermõõdu raadiuse järgi
    static double circumference (double radius) {
        return 2 * Math.PI * radius;
    }

}



