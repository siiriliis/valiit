package it.vali;

public interface LivingPlace { // elukoht
    void addAnimal(Animal animal);
    void printAnimalCounts();
    void removeAnimal (String animalType);
}
