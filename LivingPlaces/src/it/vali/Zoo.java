package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zoo implements LivingPlace {

    private List<Animal> animals = new ArrayList<Animal>();
    // Siin hoitakse infot, palju meil igat looma loomaaias on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>(); // nimekirjad on mitmuses
    // Siin hoitakse infot, palju meil igat looma loomaaeda mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Zoo() {
        // kui palju loomi mahub farmi
        maxAnimalCounts.put("Wolf", 2); //.get leiab "Pig" järgi koguse 2, ehk otsin key järgi value
        maxAnimalCounts.put("Bear", 3);
        maxAnimalCounts.put("Rabbit", 15);
    }

    @Override
    public void addAnimal (Animal animal){
        // Kas animal on tüübist Pet või pärineb sellest tüübist
        if (Pet.class.isInstance(animal)) { // kas konkreetne objekt "animal" on tüübist Pet
            System.out.println("Lemmikloomi loomaaeda vastu ei võeta");
            return;
        }
        Class animalClass = animal.getClass(); // getClass => tagasta klass,
        animalClass.isInstance(animal); // saab võrrelda, kas on samast klassist
        String animalType = animal.getClass().getSimpleName(); // getClass => tagasta klass,
        // getsimpleName klassi nimi (nt tee objektist Horse, string horse)

        // küsime, kas key on või, kui seda ei teeks, siis saaks exceptioni
        if (!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Loomaaias sellisele loomadele kohta pole"); //sellist looma pole olnud ega sa ka lisada
            return;
        }

        if (animalCounts.containsKey(animalType)) {// kui loom on olemas
            //kui palju sigu mahub
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            // kui palju veel mahub
            //.get leiab "Pig" järgi koguse 2, ehk otsin key järgi value
            int animalCount = animalCounts.get(animalType); // leia loom?
            if (animalCount >= maxAnimalCount) {
                System.out.println("Loomaias on sellele loomale kõik kohad juba täis");
                return; // kasutades returni ei ole else plokki vaja. praegune if asuks muiud else plokkis
            }
            //animals.add((FarmAnimal)animal); //lisame loomale
            // suurendan counti
            // küsin palju oli ja liidan ühe juurde ja lisan listi
            animalCounts.put(animalType, (animalCounts.get(animalType) + 1));
        }
        animals.add((FarmAnimal)animal);
        System.out.printf("Looma %s lisamine farmi õnnestus %n", animalType);

        }

        @Override
        public void printAnimalCounts () {

        }

        @Override
        public void removeAnimal (String animalType){

        }

}
