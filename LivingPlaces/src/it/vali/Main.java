package it.vali;

public class Main {

    public static void main(String[] args) {
	    //Farm livingPlace = new Farm();
		LivingPlace livingPlace = new Farm(); // kui farmi asemel kirjutan Zoo,
		// siis kõik need loomad elavad loomaias, sama forestiga

	    Pig pig = new Pig();
	   // pig.setName("Kalle");
	    //Küsime looma, kes pole farmist
		livingPlace.addAnimal(new Cat());
		//Küsime farmi looma kohta, kellele farmis kohta pole
		livingPlace.addAnimal(new Horse());
		livingPlace.addAnimal(pig);
		livingPlace.addAnimal(new Pig());
		livingPlace.addAnimal(new Pig());
		Cow cow = new Cow();
		//cow.setName("Priit");
		livingPlace.addAnimal(new Cow());
		livingPlace.addAnimal(cow);


		livingPlace.printAnimalCounts();
		System.out.println();
		livingPlace.removeAnimal("Cow");
		livingPlace.removeAnimal("Pig");
		livingPlace.printAnimalCounts();

		System.out.println();
		livingPlace.removeAnimal("Pig");
		livingPlace.printAnimalCounts();

		System.out.println();
		livingPlace.removeAnimal("Pig");
		livingPlace.printAnimalCounts();

		// mõelge ja täiendage Zoo ja Forest klasse, nii et neil oleks nende 3 meetodi sisu
    }
}
