package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Farm implements LivingPlace {
    private List<FarmAnimal> animals = new ArrayList<FarmAnimal>();
    // Siin hoitakse infot, palju meil igat looma farmis on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>(); // nimekirjad on mitmuses
    // Siin hoitakse infot, palju meil igat looma farmis mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    // teeme konstruktori
    public Farm() {
        // kui palju loomi mahub farmi
        maxAnimalCounts.put("Pig", 2); //.get leiab "Pig" järgi koguse 2, ehk otsin key järgi value
        maxAnimalCounts.put("Cow", 3);
        maxAnimalCounts.put("Sheep", 15);

    }
    @Override
    public void addAnimal(Animal animal) {
        // Kas animal on tüübist FarmAnimal või pärineb sellest tüübist
        if(!FarmAnimal.class.isInstance(animal)) {// kas konkreetne objekt "animal" on tüübist FarmAnimal
            System.out.println("Farmis saavad elada ainult farmi loomad");
            return;
        }
        String animalType = animal.getClass().getSimpleName(); //getsimpleName klassi nimi

        // küsime, kas key on või, kui seda ei teeks, siis saaks exceptioni
        if (!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Farmis sellisele loomadele kohta pole"); //sellist looma pole olnud ega sa ka lisada
            return;
        }

        if(animalCounts.containsKey(animalType)) {// kui loom on olemas
            //kui palju sigu mahub
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            // kui palju veel mahub
            //.get leiab "Pig" järgi koguse 2, ehk otsin key järgi value
            int animalCount = animalCounts.get(animalType); // leia loom?
            if (animalCount >= maxAnimalCount) {
                System.out.println("Farmis on sellele loomale kõik kohad juba täis");
                return; // kasutades returni ei ole else plokki vaja. praegune if asuks muiud else plokkis
            }
            //animals.add((FarmAnimal)animal); //lisame loomale
            // suurendan counti
            // küsin palju oli ja liidan ühe juurde ja lisan listi
            animalCounts.put(animalType,(animalCounts.get(animalType) + 1));
        }
        // Kui läheb else plokki, siis sellist looma ei ole veel farmis
        // kindlasti sellele loomale kohta on
        else {
            //animalCount = 0;
           //animals.add(animal); pean teisendama farmanimaliks
           // animals.add((FarmAnimal)animal);
            animalCounts.put(animalType, 1); //lisan looma listi
        }

        animals.add((FarmAnimal)animal);
        System.out.printf("Looma %s lisamine farmi õnnestus %n", animalType);


    }
    @Override
    //Tee meetod, mis prindib välja kõik farmis elavad loomad ning mitu neid on
    public void printAnimalCounts () {
        // Pig 2
        // Cow 3
        // Sheep 5
        for (Map.Entry<String, Integer> entry : animalCounts.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }
    @Override
    // Tee meetod, mis eemaldab farmist loomad3
    public void removeAnimal (String animalType) {
        //"Pig" on string, mitte objekt. animals.remove sab kasutada objekti korral
//        // kuidas leida nimekirjast loom kelle nimi on "Kalle"
//        for (FarmAnimal farmanimal:animals) {
//            if (farmanimal.getName().equals("Kalle")) {
//
//            }
        //ForEach
        // Teen muutuja, mis ma tahan, et iga element listist oleks  :  list mida ma tahan läbi käia
        // Farmanimal farmanimal hakkab olema järjest esimene loom, siis teine loom, kolmas ja seni kuni loomi on
        boolean animalFound = false;
        for (FarmAnimal farmanimal:animals) {
            // leiab kõik sead ja kustuttab kõik sead, kui ei pane lõppu break
            if (farmanimal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(farmanimal);
                System.out.printf("Farmist eemaldati loom %s%n", animalType);

                // Kui see oli viimane loom, siis eemalda see rida animalCounts mapist
                // muul juhul vähenda animalCounts mapis seda kogust
                if (animalCounts.get(animalType) == 1) {
                    animalCounts.remove(animalType);
                }
                else {
                    animalCounts.put(animalType,(animalCounts.get(animalType) - 1));
                }
                animalFound = true;
                break; // leiab esimese ja lõpetab tsükli
            }
        }
        // Kui ühtegi looma ei leitud
        if (!animalFound) {
            System.out.println("Farmis antud loom puudub");
        }
//        // For tsükliga sama asi
//        for (int i = 0; i <animals.size() ; i++) {
//            if (animals.get(i).getClass().getSimpleName().equals(animalType)) {
//                animals.remove((animals.get(i)));
//            }
//        }

        // täienda meetodit nii, et kui ei leitud ühtegi sellest tüübist looma
        // prindi "Farmis selline loom puudub"
    }


}
