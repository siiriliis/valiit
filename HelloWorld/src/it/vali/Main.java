package it.vali;
// public - tähendab, et klass, meetod või muutuja on avalikult nähtav/ ligipääsetav.
//			Meetodi ees kohustuslik.
// class - javas üksus, üldiselt on ka eraldi fail, mis sisaldab/grupeerib mingit funktsionaalsust.
// HelloWorld - klassi nimi, mis on ka faili nimi.
// static - seotud objektorienteeritud programmeerimisega. Meetodi ees tähendab, et seda
// 			meetodit saab välja kutsuda ilma klassist objekti loomata.
// void - meetod ei tagasta midagi.
// meetodile on võimalik kaasa anda parameetrid, mis pannakse sulgude sisse, eraldades komadega.
// String[] - tähistab stringi massiivi
// args - massiivi nimi, sisaldab käsurealt kaasa antud parameetreid.
//System.out.println() - java meetod, millega saab printida välja rida teksti.
//						See, mis kirjutatakse sulgudesse, prinditakse välja ja tehakse reavahetus.
public class Main {

    public static void main(String[] args) {

        System.out.println("Hello World!");
    }
}
