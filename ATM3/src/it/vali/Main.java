package it.vali;

import java.io.*;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {
    static String pin;

    public static void main(String[] args) {

	int balance = loadBalance();
	pin = loadPin();
	if (!validatePin()) {
        System.out.println("Kaart konfiskeeritud");
	    return;
    }

	System.out.println("Vali tehing");
	System.out.println("a) sularaha sissemakse");
	System.out.println("b) sularaha väljamakse");
	System.out.println("c) kontojääk");
	System.out.println("d)konto väljavõte");
	System.out.println("e) muuda pin");

	Scanner scanner = new Scanner(System.in);
	String answer = scanner.nextLine();
	switch (answer) {
	    case "a":
	        System.out.println("Sisesta summa");
	        int amount = Integer.parseInt(scanner.nextLine());
	        balance +=amount;
	        saveBalance(balance);
	        saveTransaction(amount, "deposit");
	        break;
        case "b":
            System.out.println("Vali summa");
            System.out.println("a) 5");
            System.out.println("b)muu summa");
            answer = scanner.nextLine();
            switch (answer) {
                case "a":
                    amount = 5;
                    if (balance < amount) {
                        System.out.println("Pole piisavalt vahendeid");
                    } else {
                        balance -= amount;
                        saveBalance(balance);
                        saveTransaction(amount, "Withdraw");
                    }
                    break;

                case "b":

                    System.out.println("Palun sisestage väljavõetav summa");
                    int cashOut = Integer.parseInt(scanner.nextLine());
                    if (cashOut < loadBalance()) {
                        balance = balance - cashOut;
                        saveBalance(balance);
                        System.out.printf("Sinu kontolt võeti %d ja jääk on %d %n", cashOut, balance);
                    }
                    else {
                    System.out.println("Kontol pole piisavalt vahendeid");
                    }
                 break;

    }
    break;


        case "d":
            printTransactions();
            break;
            default:
                break;


            }
    }





    // Toimub pin  kirjutamine pin.txt faili
    static void savePin(String pin) {
        try {
            FileWriter fileWriter = new FileWriter("pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Pin koodi salvestamine ebaõnnestus.");
        }
    }

    // Toimub pin välja lugemine pin.txt failist
    static String loadPin() {
        // toimub shadowing
        //Shadowing refers to the practice in Java programming of using
        // two variables with the same name within scopes that overlap.
        // When you do that, the variable with the higher-level scope
        // is hidden because the variable with lower-level scope overrides it.
        // The higher-level variable is then “shadowed.
        String pin = null;
        try {
            FileReader fileReader = new FileReader("pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();

        } catch (IOException e) {
            System.out.println("Pin koodi lugemine ebaõnnestus");
        }
        return pin;
    }

    static boolean validatePin () {
        Scanner scanner = new Scanner(System.in);
        pin = loadPin();
        for (int i = 0; i <3 ; i++) {
            System.out.println("Palun sisesta PIN kood");
            String enteredPin = scanner.nextLine();

            if (enteredPin.equals(pin)) {
                System.out.println("PIN on õige");
                return true;

            }

        }
        return false;
    }



    // Toimub balance kirjutamine balance.txt faili
    static void saveBalance(int balance) {

        try {
            FileWriter fileWriter = new FileWriter("balance.txt");
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus.");
        }
    }

    // Toimub balance välja lugemine balance.txt failist
    static int loadBalance() {

        int balance = 0;
        try {
            FileReader fileReader = new FileReader("balance.txt", Charset.forName("Cp1252"));
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = Integer.parseInt(bufferedReader.readLine());
            bufferedReader.close();
            fileReader.close();

        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");
        }
        return balance;
    }
    static String currentDateTimeToString() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss"); //määran formaadi
        //Get current date of calendat which point to the yesterday now
        Date date = new Date();
        // Calendar cal = Calendar.getInstance(); // kutsun välja kalendri
        // System.out.println(dateFormat.format(cal));
        return  dateFormat.format(date);
    }
    static void saveTransaction(int amount, String transaction) {

        try {
            FileWriter fileWriter = new FileWriter("transactions.txt", true);

            if (transaction.equals("deposit")) {

                fileWriter.append(String.format("%s Raha sissemaks + %d%n",currentDateTimeToString(),amount));
            }
            else {
                fileWriter.append(String.format("%s Raha väljamakse - %d%n",currentDateTimeToString(),amount));
            }

            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Tehingu salvestamine ebaõnnestus.");
        }


    }
    static void printTransactions() {
        try {
            System.out.println("Konto väljavõte:");
            FileReader fileReader = new FileReader("transactions.txt");
            //fileReader.read(); // loeb vaid ühe sümboli kaupa

            BufferedReader bufferedReader = new BufferedReader(fileReader); // loeb reakaupa

            // readLine loeb esimese rea, mis pole veel loetud. Loeb iga kord välja kutsudes järgmise rea
            String line = bufferedReader.readLine(); // loeb esimese rea
            //System.out.println(line); // prindib esimese rea
//
//            line = bufferedReader.readLine(); // loeb teise rea
//            System.out.println(line);
//
//            line = bufferedReader.readLine(); // loeb kolmanda rea
//            System.out.println(line);

            // kuidas teha seda While tsükliga:
            // kui readLine avastab, et järgmist rida tegelikult ei ole, siis tagastab meetod null
            while (line != null) {// null tähendab tühjust ehk järgmist rida pole. tühi rida teksti vahel on "" mitte null
                System.out.println(line);
                line = bufferedReader.readLine();
            }

            //Kuidas teha seda do while
//            line = null;
//            do {
//                line = bufferedReader.readLine();
//                if(line!=null) {
//                    System.out.println(line);
//                }
//            } while (line != null);

            bufferedReader.close();
            fileReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
