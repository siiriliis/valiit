package it.vali;
// import tähendab, et antud klassile Main lisatakse java class library paketile
// java.util paiknevale klassile Scanner
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //loome Scanner tüübist objekti nimega scanner, mille kaudu saab kasutaja sisendit lugeda
        Scanner scanner = new Scanner (System.in);
        System.out.println("Tere, mis on sinu nimi?");
        String name = scanner.nextLine();
        System.out.println("Tere, "+ name+"!\n" + "Aga, mis on sinu perekonnanimi?");
        String lastName =scanner.nextLine();

        //\n saab teha reavahetust
        // println - ln tagab reavahetuse. Saab kirjutada ka lihtsalt print, aga sis realõppu \n
        System.out.println("Meeldiv tutvuda "+name +" "+ lastName+ "!\n" + "Mis on sinu lemmikvärv?");
        String colour= scanner.nextLine();
        System.out.println(colour+ " on ilus värv!");
        System.out.println("Meeldiv tutvuda "+name +" "+ lastName+ "! \n" + "Sinu lemmikvärv on "
                + colour +"\n\n\n");

        //kasutades printf ja %s saab kirjutada lause ilma jutumärkideta ja plussideta, lõppu tuleb reavahetuseks panna \n
        System.out.printf("Meeldiv tutvuda %s %s! \nSinu lemmikvärv on %s\n", name, lastName, colour);

        // StringBuilder on sama sisu pikalt, mis printf
        StringBuilder builder = new StringBuilder();
        builder.append("Meeldiv tutvuda ");
        builder.append(name);
        builder.append(" ");
        builder.append(lastName);

        //\n saab teha reavahetust
        builder.append("! "+"\n");
        builder.append("Sinu lemmikvärv on ");
        builder.append(colour);

        //String fullText =builder.toString();
        //System.out.println(fullText);

        System.out.println(builder.toString());

        //System.out.printf kasutab enda sisemiselt SringBuilderit
        //String.format kasutab ka sisemiselt StringBuilderit
        String text = String.format("Meeldiv tutvuda %s %s! Sinu lemmikvärv on %s",
                name, lastName, colour);
        System.out.println(text);
    }
}
