package it.vali;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Map<String, String> map = new LinkedHashMap<>(); //LinkedHasMap annab õige lisamise järjekorra, HashMap annab suvalises järjekorras
        //LinkedHashMap<String, String> map = new LinkedHashMap<>(); //LinkedHasMap annab õiges järjekorras, HashMap annab suvalises järjekorras
        //java nimetab map, teised dictionary

        //Map järjekorda ei salvesta
        // Sõnaraamat
        // key => value
        // Maja => House
        // Isa => Dad
        // Puu => Tree

        // put == add. Lisame listi map
        map.put("Maja", "House");
        map.put("Ema", "Mother");
        map.put("Isa", "Father");
        map.put("Puu", "Tree");
        map.put("Sinine", "Blue");
        for (Map.Entry<String, String> entry:map.entrySet()) {
            System.out.println(entry.getKey());

        }

        // oletame, et tahan teada, mis on inglise keeles Puu
        // get tagastab stringi
        String translation = map.get("Puu");
        System.out.println(translation);

        Map<String, String> idNumberAndNameMAp = new HashMap<String, String>();//LinkedHasMap annab õiges järjekorras, HashMap annab suvalises järjekorras
        idNumberAndNameMAp.put("48307300234", "Malle");
        idNumberAndNameMAp.put("38307300234", "Kalle");
        idNumberAndNameMAp.put("38307300234", "Kalev"); // Kalev kirjutab Kalle üle
        // Kui kassutada put sama key lisamisel, kirjutatakse value üle.
        System.out.println(idNumberAndNameMAp.get("38307300234"));
        idNumberAndNameMAp.remove("38307300234");
        System.out.println(idNumberAndNameMAp.get("38307300234"));

        // EST => Estonia
        // Estonia => +372

        // Loe lauses üle kõik erinevad tähed
        // ning prindi välja ja iga tähe järel,
        // mitu tükki teda selles lauses oli

        // tüüp char
        // char on selline tüüp, kus saab hoida ükskiut sümbolit
        char symbolA = 'a';
        char symbolB = 'b';
        char newLine = '\n';


        String sentence = "elas metsas mutionu";

        // e 2
        // l 1
        // a 2
        // s 3
        //...
        Map <Character, Integer> letterCounts = new HashMap<Character, Integer>();

        char [] characters = sentence.toCharArray();

        for (int i = 0; i <characters.length ; i++) {
            if (letterCounts.containsKey(characters [i])) {
                letterCounts.put(characters[i], letterCounts.get(characters[i])+1);
            }
            else {
                letterCounts.put(characters[i], 1);
            }
        }
        System.out.println(letterCounts);

        // Map.Entry<Character, Integer> on klass, mis hoiab endas ühte rida map-is
        // ehk ühte key value kombinatsiooni
        for (Map.Entry<Character, Integer> entry : letterCounts.entrySet()) {
            System.out.printf("Tähte %s esines %d korda %n", entry.getKey(), entry.getValue());
        }






    }
}
