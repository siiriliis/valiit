package it.vali;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//	 //Program küsib kas tahate jätkate
//        //variant 1 => while tsükkel
//
//        // Jätkame seni, kuni kasutaja kirjutab "ei"
//        Scanner scanner = new Scanner (System.in);
//        String inputAnswer = scanner.nextLine();
//
//        String answer = "ei";
//        System.out.println("Kas tahad jätkata? Jah/Ei ");
//
//        while ( !inputAnswer.toLowerCase().equals(answer.toLowerCase())) {
//            System.out.println("Kas tahad jätkata? Jah/Ei ");
//            inputAnswer = scanner.nextLine();
//        }
//        System.out.println("Sa ei soovi jätkata ");


        //Do while tsükkel on nagu while tskkel ainult, et kontroll tehakse peale esimest kordust
        //variant 2 => Do while loop tsükkel
//        Scanner scanner = new Scanner (System.in);
//        String inputAnswer; // do sisse ei saa stringe panna vaid aint muutujat
//        String answer = "ei";
//        do {
//            System.out.println("Kas tahad jätkata? Jah/Ei ");
//            inputAnswer = scanner.nextLine();
//        }
//        while(!inputAnswer.toLowerCase().equals(answer.toLowerCase()));
//        }
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int number = random.nextInt(5) + 1;

        do {
            System.out.println("Arva ära üks number 1st 5ni");
            int inputNumber = Integer.parseInt(scanner.nextLine());

            while (inputNumber != number) {
                System.out.println("Proovi uuesti");
                inputNumber = Integer.parseInt(scanner.nextLine());
            }
            System.out.println("Arvasid ära");
            System.out.println("Kas soovid veel mängida?");

        }
        while (!scanner.nextLine().toLowerCase().equals("ei"));
        System.out.println("oli tore");
    }
}






