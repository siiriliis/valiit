package it.vali;

import java.awt.*;

public class Main {

    public static void main(String[] args) {
        // kui pole vääärtust andnud siis
        // int vaikeväärtus 0
        // booleani vaikeväärtus false
        // double vaikeväärtus 0.0
        // stringi vaikeväärtus null
        // objektide (Monitor, FileWriter) vaikeväärtus null
        // massiivide int [] vaikeväärtus null

        Monitor firstMonitor = new Monitor();
        firstMonitor.setDiagonal(-1000);
        //firstMonitor.color = Color.GRAY; seda ei saa kasutada, sest color on private
        System.out.println(firstMonitor.getDiagonal());

        firstMonitor.setDiagonal(200);
        System.out.println(firstMonitor.getDiagonal());

        firstMonitor.setDiagonal(20);
        System.out.println(firstMonitor.getDiagonal());

        System.out.println(firstMonitor.getYear());

        firstMonitor.setManufacturer("Huawei");
        firstMonitor.printInfo();
        //System.out.println(firstMonitor.getManufacturer());
        firstMonitor.setManufacturer("");
        firstMonitor.setManufacturer(null);

    }
}
