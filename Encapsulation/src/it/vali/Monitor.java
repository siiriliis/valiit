package it.vali;
// kapseldamine -> siin näitame, mis on kapseldamine

// enum on tüüp, kus saab defineerida erinevaid lõplikke valikuid
// tegelikult salvestatakse enum alati int-ina
enum Colour {
    BLACK, WHITE, GREY
}

enum ScreenType {
    LCD, TFT, OLED, AMOLED
}

// sama klassi sees on privat muutujad meetotidele näha
public class Monitor {
    //private tähendab, et ei ole väljapoole nähtavad
    private String manufacturer;
    private double diagonal; // tollides "
    //String color;
    private Colour color;
    private ScreenType screenType;
    private int year = 2000;
    public int getYear() { // kõik monitorid on aastast 2000.
        return year;
    }

    //•	code -> generate -> getter and setter
    // get ja set käivad paaris
    public double getDiagonal () { // tagastab diagonal
        if (diagonal == 0) {
            System.out.println("Diagonaal on seadistamata");
        }
        return diagonal; // et saaksin kasutada private objekti.
    }
    public void setDiagonal (double diagonal) { // ei tagasta, seadistab
        // this tähistab seda konkreetsed objekti
        if (diagonal < 0) {
            System.out.println("Diagonaal ei saa olla negatiivne");
        }
        else if (diagonal > 100) {
            System.out.println("Diagonaal ei saa olla suurem kui 100\"");
        }
        else {
            this.diagonal = diagonal;
        }

    }
    // ära luba seadistada monitori tootjaks Huawei
    // kui keegi soovib seda tootjat monitori tootjaks panna
    // pannakse hoopiks tootjaks tekst "Tootja puudub"
    // keela ka tühja tootjanime lisamine "", null

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        if (manufacturer == null || manufacturer.equals("Huawei") || manufacturer.equals("") ) { // null peab kõige ees olema, sest kui on null,
                                                                                                // siis ei saa välja kutsuda ühtegi meetodit, st ka .equalsi
            //System.out.println("Tootja puudub");
           this.manufacturer = "Tootja puudub";
        } else {
            this.manufacturer = manufacturer;
        }
    }

    public Colour getColor() {

        return color;
    }

    public void setColor(Colour color) {

        this.color = color;
    }

    // Kui ekraanitüüp on seadistamata (null), siis tagasta tüübiks LCD
    public ScreenType getScreenType() {
        if (screenType == null) {
            return ScreenType.LCD;
        }
        else {
            return screenType;
        }

    }

    public void setScreenType(ScreenType screenType) {
        this.screenType = screenType;
    }

    // teeme klassi meetodid
   public void printInfo () { //default on public
        System.out.println();
        System.out.println("Monitori info:");
        System.out.printf("Tootja: %s%n", manufacturer);
        System.out.printf("Diagonaal: %.1f%n", diagonal);
        System.out.printf("Värv: %s%n", color);
        System.out.printf("Ekraani tüüp: %s%n", getScreenType()); // küsib läbi meetodi
       System.out.printf("Aasta: %d%n ", year);
        System.out.println();

    }
// tee meetod, mis tagastab ekraani diagonaali cm-tes
//    void diagonalCm () {
//        diagonal = diagonal*2.54;
//        System.out.printf("Diagonaal on %.1f cm %n", diagonal);
//    }
    public double getDiagonalToCm() {
        return diagonal*2.54;
    }

}
