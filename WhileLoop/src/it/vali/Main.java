package it.vali;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {
        // While tsükkel on tsükkel, kus korduste arv ei ole teada

//      Lõpmatu tsükkel kasutades while
//	    while (true) {
//	        System.out.println("Tere!");
//      }

        //Programm mõtleb numbri
        // Programm küsib kasutajalt arva number ära
        // Senikaua kuni kasutaja arvab valesti, ütleb proovi uuesti
        // Kasutaja peab ära arvama numbri 1-10ni
        Scanner scanner = new Scanner (System.in);

        Random random = new Random();
        //random.nextInt(5) genereerib numbri 0 kuni 4
        int a = random.nextInt(5) + 1;
        System.out.println("Mõtlesin numbri. Arva ära üks number 0st 5ni)");
        //variant 2
        int randomNumber = ThreadLocalRandom.current().nextInt(1,6);

        int number = 7;
        System.out.println("Mõtlesin numbri. Arva ära üks number vahemikus 1-10)");
        int guessNumber = Integer.parseInt(scanner.nextLine());

        while (guessNumber != number){
            System.out.println("See on vale number. Proovi uuesti");
            guessNumber = Integer.parseInt(scanner.nextLine());
//            if (guessNumber == number) {
//                System.out.println("Tubli! Arvasid ära");
//            }
            }
        System.out.println("Tubli! Arvasid ära");
        }
    }
