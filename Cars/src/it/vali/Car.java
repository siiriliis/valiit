package it.vali;

import java.util.ArrayList;
import java.util.List;

enum Fuel {
    GAS, DIESEL, PETROL, HYBRID, ELECTRIC
}

public class Car {

    // klassi Car muutujad:
    private String make;
    private String model;
    private String color;
    private int year;
    private Fuel fuel; // Enumiga
    private boolean isUsed;
    private boolean isEngineRunning; //vaikimisi false
    private int speed;
    private int maxSpeed;
    private Person owner;
    private Person driver;
    private int maxPersons;
    private List<Person> passangers = new ArrayList<Person>();


    //private Person [] owners = new int[] => array ei ole hea variant, sest peab teadma, mitu omaniku on

    // klassi konstruktorid (constructor)
    // on eriline meetod, mis käivitatakse klassist objekti loomisel
    // Main meetodis Car() kutsub välja konstruktori.
    // new FileTeader("input.txt") => input.txt on ka konstruktor
    // saan panna siia selle, mida tahan, et juhtuks kui kutsutakse välja klass Car ( Car bmw = new Car();)
    // kasutatakse selleks, et anda algväärtusi
    public Car(){
        System.out.println("Loodi auto objekt");
        maxSpeed = 200;
        isUsed = true; // vaikimisi on kõik autod kasutatud
        fuel = Fuel.PETROL;
    }

    // Constructor overloading
    public Car (String make, String model, int year, int maxSpeed) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
    }
    public Car (String make, int year, Person owner, Person driver) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
    }

    //Constructor fuel, isUsed
    public Car (Fuel fuel, boolean isUsed) {
        this.fuel = fuel;
        this.isUsed = isUsed;
    }

    // klassi car meetodid
    //teeme meetodi auto käivitamiseks
    public  void startEngine() {
        // Kui auto ei tööta, pane tööle
        if (!isEngineRunning) {
            isEngineRunning = true;
            System.out.println("Mootor käivitus");
        }
        else {
            System.out.println("Mootor juba töötab");
        }
    }

    // teeme meetodi mootori peatamiseks
    public void stopEngine () {
        if (isEngineRunning) {
            isEngineRunning = false;
            System.out.println("Mootor seiskus");
        }
        else {
            System.out.println("Mootor ei tööta");
        }
    }

    // kiirendame etteantud kiiruseni
    // kontrollime lubatud sõidukiirust. See ei saa olla auto parameetrites. peaks olema sõidutee klass juures. ei tee praegu
    public void accelerate(int targetSpeed) {
        if (!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa kiirendada");
        }
        else if (targetSpeed > maxSpeed) {
            System.out.printf("Auto nii kiiresti ei sõida. Maksimum kiirus on %d%n", maxSpeed);
        }
        else if (targetSpeed < 0) {
            System.out.println("Kiirus peab olema suurem kui 0 km/h");
        }
        else if (targetSpeed < speed) {
            System.out.println("Auto ei saa kiirendada väiksemale kiirusele");
        }
        else {
            speed = targetSpeed;
            System.out.printf("Auto kiirendas %d km/h kiiruseni%n", speed); // speed asemel võib panna ka targetSpeed
        }
    }
    // teeme meetodi kiiruse aeglustamiseks
    public void slowDown (int targetSpeed) {
        if (!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa aeglustada");
        }
        else if (targetSpeed == 0) {
            System.out.println("Auto on peatunud");
        }
        else if (targetSpeed < 0) {
            System.out.println("Auto kiirus ei saa olla negatiivne");
        }
        else if (targetSpeed > speed) {
            System.out.println("Auto ei saa aeglustada suuremale kiirusele");
        }
        else {
            speed = targetSpeed;
            System.out.printf("Auto aeglustas %d km/h kiiruseni%n", speed); // speed asemel võib panna ka targetSpeed
        }
    }
    // teeme parkimise meetodi, kutsudes välja juba olemasolevaid meetodeid
    public void park() {
        if (isEngineRunning) {
            slowDown(0);
            stopEngine();
            System.out.println("Auto on pargitud");
        }
        else {
            System.out.println("Auto on pargitud");
        }
    }

    @Override
    public String toString() {
        return make + " " + model;
    }

    // lisame sõitja
    public void addPassengers(Person passenger) {
        if (passangers.size ()< maxPersons) {
            if (passangers.contains(passenger)) {
                System.out.printf("%s juba on autos%n", passenger.getFirstName());
                //return;
            }else {
                passangers.add(passenger);
                System.out.printf("Autosse lisati reisija %s%n", passenger.getFirstName());
            }

        }
        else {
            System.out.println("Auto on täis");
        }
    }

    // eemaldame sõitja
    public void removePassenger (Person passenger) {
        if (passangers.indexOf(passenger) != -1) {
            System.out.printf("Autost eemaldati reisija %s%n", passenger.getFirstName());
            passangers.remove(passenger);
        }
        else {
            System.out.println("Autos sellist reisijat ei ole");
        }
    }
    // näita reisijaid
    public void showPassengers() {
        //Foreach loop/tsükkel
        // iga elemendi kohta listis passengers tekita objekt passenger
        // 1.kordus Person passenger on esimene reisija
        // 2.kordus Person passenger on teine reisia
        System.out.println("Autos on järgnevad reisijad: ");
        for (Person passenger: passangers) {
            System.out.println(passenger.getFirstName());
        }
    }

    // Lisame reisija
//    public void addPassengers (Person passanger) {
//        if (passangers.size() >= 4) {
//            System.out.println("Autosse ei mahu rohkem");
//        }
//        else {
//            passangers.add(passanger);
//            System.out.println("lisandus 1 sõitja");
//        }
//    }
        //Eemaldame reisja
//    public void removePassangers(Person passanger) {
//        if (passangers.size() == 0) {
//            System.out.println("Autos pole rohkem inimesi");
//        }else {
//            passangers.remove(passanger);
//            System.out.println("1 sõitja lahkus");
//        }
//    }
        //näita reisijaid
//    public void showPassengerList () {
//        System.out.println("Autos on: ");
//        for (int i = 0; i <passangers.size() ; i++) {
//            System.out.println(passangers.get(i).getFirstName());
//        }
//    }

    public int getMaxPersons() {
        return maxPersons;
    }

    public void setMaxPersons(int maxPersons) {
        this.maxPersons = maxPersons;
    }


    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public Person getDriver() {
        return driver;
    }

    public void setDriver(Person driver) {
        this.driver = driver;
    }
    public List<Person> getPassangers() {
        return passangers;
    }

    public void setPassangers(List<Person> passangers) {
        this.passangers = passangers;
    }


    // lisa autole parameetrid driver ja owner ja nendele siis get ja set meetodid
    // loo mõni auto objekt, kelle on siis määratud kasutaja ja omanik
    // Prindi välja auto omaniku vanus

    // Lisa autole max reisijate arv
    // Lisa autole võimalus hoida reisijaid
    // Lisa meetodid reisijate lisamiseks ja eemaldamiseks autost
    // Kontrolli ka, et ei lisaks rohkem reisijaid kui mahub

}
