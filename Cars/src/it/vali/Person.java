package it.vali;

enum Gender {
    FEMALE, MALE, NOTSPECIFIED
}

public class Person {


    private String firstName;
    private String lastName;
    private Gender gender;
    private int age;
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("Eesnimi on: %s, perekonna nimi on: %s, vanus on: %d%n", firstName, lastName, age);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    //loome konstruktori.
    // tunneb ära, et ei ole peale publicu ja nime midagi
    // kui klassil ei ole defineeritud konstruktorit,
    // siis tegelikult tehakse nähtamatu parameetrite konstruktor, mille sisu on tühi
    // kui klassile ise lisada mingi konstruktor,
    // siis see nähtamatu parameetrita konstruktor kustutatakse
    // Sellest klassist saab siis teha objekti ainult selle uue konstruktoriga

    public Person (String firstName, String lastName, Gender gender, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;

    }
    public Person(String firstName) {
        this.firstName = firstName;
    }
}
