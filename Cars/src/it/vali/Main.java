package it.vali;

public class Main {

    // class car -> klass auto, sisaldab auto parameetreid
    //new Car() -> konstruktor
    //bmw.startengine -> objekti meetod
    public static void main(String[] args) {
	    Car bmw = new Car(); // kutsume välja klassi Car meetod Car.
        // Car() kutsub välja konstruktori. new FileTeader("input.txt") => input.txt on ka konstruktor
        // Prindib välja: Loodi auto objekt <= kutsuti välja klassist Car

        // Kutsume välja meetodid startEngine ja stopEngine
        bmw.startEngine();// Mootor käivitub
        bmw.startEngine(); // Mootor juba töötab
        bmw.stopEngine(); // Mootor seiskub
        bmw.stopEngine(); // Mootor juba ei tööta

        bmw.accelerate(100); //kiirendame. mootor ei tööta
        bmw.startEngine(); // käivitame mootori
        bmw.accelerate(100); // Auto nii kiiresti ei sõida. Maksimum kiirus on 0

        Car fiat = new Car(); //konstruktur kustub välja: Loodi auto objekt
        Car mercedes = new Car(); //konstruktur kustub välja: Loodi auto objekt
        Car opel = new Car("Opel", "Vectra", 1999, 205);
        opel.startEngine();
        opel.accelerate(210);
        Car toyota = new Car(Fuel.PETROL, true);

        // Person pesron = new Person(); // nähtamatu konstruktor kustutati sel kui lõin ise konstruktori
        Person person = new Person("Kati", "Karu", Gender.FEMALE, 23);
        System.out.println();

        toyota = new Car();
       // toyota = new Car("toyota", "yaris", 2010, 200);
        toyota.startEngine();
        toyota.accelerate(50);
        toyota.slowDown(5);
        toyota.park();
        toyota.park();

        //Person owner = new Person("Liis", "Karu", Gender.FEMALE, 20);
        //Person driver = new Person("Mati", "Karu", Gender.MALE, 22);
        //bmw = new Car ("opel", 2015, owner, driver );

        Person otherPerson = new Person("Mati", "Karu", Gender.MALE, 23);
        opel.setDriver(person);
        opel.setOwner(otherPerson);
        System.out.printf("Sõiduki %s omaniku vanus on %d%n", opel.getMake(), opel.getOwner().getAge());
        System.out.println();

        Person person1 = new Person("Liis");
        Person person2 = new Person("Liisa");
        Person person3 = new Person("Helen");
        Person person4 = new Person("Katrin");
        Person person5 = new Person("Martin");

        opel.setMaxPersons(4);
        opel.addPassengers(person);
        opel.addPassengers(otherPerson);
        opel.addPassengers(person1);
        opel.addPassengers(person1);
        opel.addPassengers(person2);
        opel.addPassengers(person3);


        opel.removePassenger(person);
        opel.addPassengers(person4);
        opel.addPassengers(person5);
        opel.showPassengers();

        Person juku = new Person("Juku", "Juurikas", Gender.MALE, 23);
        Person malle = new Person("Malle", "Juurikas", Gender.FEMALE, 23);
        System.out.println(juku); // tegin toStringi, et nii teha saaks
        System.out.println(malle);// tegin toStringi, et nii teha saaks
        System.out.println(opel); // toStringi on kasuatud, et seda teha saaks

    }
}
