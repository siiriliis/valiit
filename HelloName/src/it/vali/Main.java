package it.vali;

public class Main {

    public static void main(String[] args) {
        //Deklareerimine/defineerime tekstitüüpi (String) muutuja (variable),
        // mille nimeks paneme name ja väärtuseks Siiri
	    String name = "Siiri";
	    String secondName = "Liis";
	    System.out.println("Hello " + name+"!");
	    System.out.println("Hello "+ name + "-"+ secondName+"!");
    }
}
