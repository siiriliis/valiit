package it.vali;

// Bird klass kasutab/implementeerib liidest/interface Flyer
public class Bird implements Flyer {
    @Override
    public void fly() {
        jumpUp();
        System.out.println("Lind lendab");
    }

    private void jumpUp (){ // private tähendab, et saan teises meetodis
        // seda meetodit välja kutsuda, aga eraldi jumpUp meetodit väljakutsuda ei saa
        System.out.println("Lind hüppas õhku");
    }
}
