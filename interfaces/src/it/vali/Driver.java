package it.vali;

// Mõtle kas lennuk ja auto võiks mõlemad kasutada Driver liidest?
// Driver liides võiks sisaldada 3 meetodi definitsiooni/kirjeldust (meetod ise on ALATI klassi sees):
// int getMaxDistance()
// void drive()
// void stopDriving(int afterDistance).
// Pane auto ja lennuk mõlemad kasutama seda liidest

// interfaces määran ainult:
//tagastusväärtuse
//meetodi nime
//meetodi parameetri
public interface Driver {
    int getMaxDistance();
    void drive();
    void stopDriving(int afterDistance);
}
