package it.vali;

public class Car extends Vehicle implements Driver {
    private int maxDistance;
    private String make;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }



    public Car (int maxDistance) {
        this.maxDistance = maxDistance;
    }

    public Car() {
        this.maxDistance = 600; // kui kaasa ei anna, siis vaikimisi on 600
    }

    @Override
    public String toString() {
        return  "Auto andmed: " + make + " " + maxDistance;
    }

    @Override
    public int getMaxDistance() {

        return maxDistance;
    }

    @Override
    public void drive() {
        System.out.println("Auto sõidab");

    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Auto lõpetab sõitmise %d km pärast %n", afterDistance);

    }
}
