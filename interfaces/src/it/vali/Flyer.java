package it.vali;

// interface ehk liides, suunnib seda kasutavat/implementeerivat klassi
// omama liideses kirja pandud meetodeid
// (sama tagastustüübiga ja sama parameetrite kombinatsiooniga)

// iga klass, mis interface kasutab määrab ise ära meetodi sisu
public interface Flyer {
    void fly(); //tagastusväärtus ja parameeter, sisu siin pole. Sisu tuleb klasside Plane ja Bird sees
}
