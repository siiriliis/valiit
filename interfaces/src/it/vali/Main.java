package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// write your code here
        // teeme interface tüüpi objektid
        Flyer bird = new Bird(); //muutuja tüübiks on interface Flyer, muutuja bird
        Flyer plane = new Plane();

        bird.fly();
        System.out.println();
        plane.fly();

        List<Flyer> flyers = new ArrayList<Flyer>();
        flyers.add(bird);
        flyers.add(plane);

        Plane boeing = new Plane();
        Bird sparrow = new Bird();

        flyers.add(boeing);
        flyers.add(sparrow);

        System.out.println(); // tühi rida

        for (Flyer flyer :flyers) {
            flyer.fly();
            System.out.println();
        }

        Driver bmw = new Car();
        Car newbmw = new Car();
        Driver secondBoeing = new Plane();

        bmw.getMaxDistance();
        System.out.println();
        bmw.drive();
        System.out.println();
        bmw.stopDriving(50);
        newbmw.getMaxDistance();
        newbmw.drive();
        newbmw.stopDriving(100);

        // Lisa liides Driver, klass Car.

        // Lisa mootorrattas

    }
}
