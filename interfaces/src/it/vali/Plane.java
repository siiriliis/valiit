package it.vali;

// Plane pärineb klassist Vehicle ja kasutab /implementeerib liidest Flyer
public class Plane extends Vehicle implements Flyer,Driver  { // järjekord on oluline
    private int maxDistance = 2000;

    @Override
    public void fly() {
        doCheckList();
        startEngine();
        System.out.println("Lennuk lendab");
    }
    private void doCheckList () {
        System.out.println("Lennuki meeskond täidab check listi enne käivitamist");
    }
    private void startEngine () {
        System.out.println("Mootor käivitatakse");
    }

    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    public void drive() {
        System.out.println("Lennuk sõidab");

    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Lennuk lõpetab sõitmise %d km pärast %n", afterDistance);

    }
}
