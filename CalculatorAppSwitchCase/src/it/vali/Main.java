package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        do{ // kordab kogu mängu kui tahan
            scanner = new Scanner(System.in);
            System.out.println("Palun sisesta esimene arv");
            int a = Integer.parseInt(scanner.nextLine());
            System.out.println("Palun sisesta teine arv");
            int b =Integer.parseInt(scanner.nextLine());
            //String option; // on vaja siis kui alla paneksin while (answer.equals("a") & answer.equals("b") jne) booleani asemel
            boolean wrongAnswer;
            do { // kordab tehet, kui tehe valesti valitud
                System.out.printf("Vali tehe:\n" +
                        "a)liitmine\n" +
                        "b)lahutamine\n" +
                        "c)korruatmine\n" +
                        "d)jagamine\n ");
                String option = scanner.nextLine();
                wrongAnswer = false;


//                if (option.equals("a")) {
//                    System.out.printf("Numbrite %d ja %d summa on %d%n",a,b,calculateSum(a,b));
//                }
//                else if (option.equals("b")) {
//                    System.out.printf("Numbrite %d ja %d vahe on %d%n",a,b, subtract(a,b));
//                }
//                else if (option.equals("c")) {
//                    System.out.printf("Numbrite %d ja %d korrutis on %d%n",a,b, multiply(a,b));
//                }
//                else if (option.equals("d")){
//                    System.out.printf("Numbrite %d ja %d jagatis on %.2f%n",a,b, divide(a,b));
//                }
//                else {
//                    System.out.println("Selline tehe puudub.");
//                    wrongAnswer = true;
//                }

                // Switch case konstruktsioon sobib kasutamiseks if ja else if asemel
                // siis kui if ja else if kontrollivad ühe ja sama muutuja väärtust
                switch (option) {
                    case "1":
                    case "A": // saan kontrollida nii väiest kui suurt tähte ja teeb sama asja mõlemal juhul
                    case "a":
                        System.out.printf("Numbrite %d ja %d summa on %d%n",a,b,calculateSum(a,b));
                        break;
                    case "B":
                    case "b":
                        System.out.printf("Numbrite %d ja %d vahe on %d%n",a,b,subtract(a,b));
                        break;
                    case "c":
                        System.out.printf("Numbrite %d ja %d korrutis on %d%n",a,b,multiply(a,b));
                        break;
                    case "d":
                        System.out.printf("Numbrite %d ja %d vahe on %.2f%n",a,b,divide(a,b));
                        break;
                    default:
                        System.out.println("Selline tehe puudub.");
                        wrongAnswer = true;
                        break;
                }

            } while (wrongAnswer); // while (answer.equals("a") & answer.equals("b") jne on ka võimalus

            System.out.println("Kas sa soovid veel tehteid teha? Jah/Ei");

        }

        while (scanner.nextLine().toLowerCase().equals("jah"));
        System.out.println("Oli tore");


    }
    static int calculateSum (int a, int b) {
        int sum = a + b; // selle võib kirjutamata jätta
        return sum;
    }


    static int subtract (int a, int b) {
        return a -b ;
    }


    static int multiply (int a, int b) {
        return a * b;
    }


    static double divide (int a, int b) {
        return(double) a / b;// üks täisarv peaks olema tehtud doubleks
    }

}


