package it.vali;

public class Main {

    public static void main(String[] args) {
        // Abstraktne klass on hübriid liidesest ja klassist
        //Selles klassis saab defineerida nii meetodite struktuure (nagu liideses),
        // aga saab ka defineerida meetodi koos sisuga.
        // Abstarktsest klassit ei saa otse objekti luua, saab ainult pärineda


        ApartmentKitchen kitchen = new ApartmentKitchen();
        kitchen.setHeight(100);
        kitchen.becomeDirty();

    }
}
