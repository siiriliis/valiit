package it.vali;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
	// küsime kasutajalt emaili
        System.out.println("Sisesta email");
        Scanner scanner = new Scanner(System.in);
        String regex = "([a-z0-9\\._]{1,50})@[a-z0-9]{1,50}\\.([a-z]{2,10})\\.?([a-z]{2,10})?";

        String email = scanner.nextLine();
        if (email.matches(regex)) {
            System.out.println("Email oli korrektne");
        }
        else{
            System.out.println("email ei ole korrektne");
        }
        Matcher matcher = Pattern.compile(regex).matcher(email);
        if (matcher.matches()) {
            System.out.println("Kogu email: " + matcher.group(0));
            System.out.println("Teks vasakulpool @ märki: " + matcher.group(1));
            System.out.println("Domeeni laiend: " + matcher.group(2));
            //Sisesta email
            //siiri@gmail.com
            //Email oli korrektne
            //Kogu email: siiri@gmail.com
            //Teks vasakulpool @ märki: siiri
            //Domeeni laiend: com
        }
        // Küsi kasutajalt isikukood ja valideeri, kas see on õiges formaadis
        // aasta peab olema reaalne aasta, kuu number, kuupäeva number
        // prindi välja tema sünnipäev
        String secondRegex ="([3-6]{1})([0-9]{2})(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])([0-9]{4})";
        boolean idTrue;
        do {
            idTrue = false;
            System.out.println("Sisesta oma isikukood");
            String id = scanner.nextLine();

            if (id.matches(secondRegex)) {
                System.out.println("isikukood on korrektne");
                idTrue = true;
                return;
            }
            else{
                System.out.println("isikukood ei ole korrektne");
            }
        } while (true);


    }
}
