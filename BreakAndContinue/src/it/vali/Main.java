package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Küsime kasutajalt pinkoodi, kui see on õige, ütleme "õige"
        // muul juhul küsime uuesti. Kokku Küsime 3 korda

        Scanner scanner = new Scanner(System.in);
        String pin = "1234";

        //variant 1 for korral
        for (int i = 0; i < 3; i++) {
            System.out.println("Sistesta oma PIN:");
            String inputPin = scanner.nextLine();

            if (inputPin.equals(pin)) {
                System.out.println("PIN õige!");
                break; // break hüppab tsüklist välja
            }

        }
        System.out.println("katsed said läbi");


        // prindi välja 10 kuni 20 ja 40 kuni 60
        //continue jätab selle tsükli korduse katki ja läheb järgmise korduse juurde

        for (int i = 10; i <=60 ; i++) {
            if ( i> 20 && i < 40) {
                continue; // jätab vahele numbrid 21-39
            }
            System.out.println(i);
        }
    }
}