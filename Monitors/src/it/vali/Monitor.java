package it.vali;


// enum on tüüp, kus saab defineerida erinevaid lõplikke valikuid
// tegelikult salvestatakse enum alati int-ina
enum Colour {
    BLACK, WHITE, GREY
}

enum ScreenType {
    LCD, TFT, OLED, AMOLED
}

public class Monitor { // nii ei tohiks klassi teha!!!!
    String manufacturer;
    double diagonal; // tollides "
    //String color;
    Colour color;
    ScreenType screenType;

// teeme klassi meetodid
    void printInfo () {
        System.out.println();
        System.out.println("Monitori info:");
        System.out.printf("Tootja: %s%n", manufacturer);
        System.out.printf("Diagonaal: %.1f%n", diagonal);
        System.out.printf("Värv: %s%n", color);
        System.out.printf("Ekraani tüüp: %s%n", screenType);
        System.out.println();

    }
// tee meetod, mis tagastab ekraani diagonaali cm-tes
//    void diagonalCm () {
//        diagonal = diagonal*2.54;
//        System.out.printf("Diagonaal on %.1f cm %n", diagonal);
//    }
    double getDiagonalToCm() {
        return diagonal*2.54;
    }

}
