package it.vali;

public class Main {

    public static void main(String[] args) {
	    Monitor firstmonitor = new Monitor(); // saan luua objekti monitor 1
        Monitor secondMonitor = new Monitor();// saan luua objekti monitor 2
        Monitor thirdMonitor = new Monitor();

        firstmonitor.manufacturer = "Philips"; // punktiga saan välja kutsuda meetodeid klassist monitor. annan väärtuse
        System.out.println(firstmonitor.manufacturer);// küsin väärtuse
        firstmonitor.color = Colour.BLACK; // panen Color. ja saan valikud
        firstmonitor.diagonal = 27;
        firstmonitor.screenType = ScreenType.LCD;

        secondMonitor.manufacturer = "Dell";
        secondMonitor.color = Colour.WHITE;
        secondMonitor.diagonal = 24;
        secondMonitor.screenType = ScreenType.OLED;

        thirdMonitor.manufacturer = "Samsung";
        thirdMonitor.color = Colour.GREY;
        thirdMonitor.diagonal = 32;
        thirdMonitor.screenType = ScreenType.TFT;



        System.out.println(secondMonitor.screenType);

        firstmonitor.color = Colour.WHITE;

        // teeme monitoridest massiivi
        // int [] numbers = new int [2]; // numbrite massiivi tegemine
        Monitor [] monitors = new  Monitor[4];
        //Lisa massiivi 4 monitori
        // Prindi välja kõigi monitoride tootja, mille diagonaal on suurem kui 25 tolli
        monitors [0] = firstmonitor;
        monitors [1] = secondMonitor;
        monitors [2] = thirdMonitor;
        monitors [3] = new Monitor();
        monitors [3].manufacturer = "Sony";
        monitors [3].color = Colour.WHITE;
        monitors [3].diagonal = 50;
        monitors [3].screenType = ScreenType.LCD;

        for (int i = 0; i < monitors.length; i++) {
            if (monitors[i].diagonal > 25)
                System.out.println(monitors[i].manufacturer);

        }

//         //Leia monitori värv kõige suuremal monitoril

//        double max = monitors[0].diagonal;
//        Colour colour = monitors[0].color;
//        for (int i = 0; i < monitors.length; i++) {
//            if (monitors[i].diagonal > max) {
//                max = monitors[i].diagonal;
//
//                System.out.println(max);
//                System.out.println(monitors[i].color);
//            }
//        }

        Monitor maxSizeMonitor = monitors[0];
        for (int i = 0; i <monitors.length ; i++) {
            if(monitors[i].diagonal > maxSizeMonitor.diagonal)
                maxSizeMonitor = monitors[i];

        }
//        System.out.println(maxSizeMonitor.color);
//        System.out.println(maxSizeMonitor.manufacturer);
//        System.out.println(maxSizeMonitor.screenType);
        maxSizeMonitor.printInfo();
        firstmonitor.printInfo(); // punkiga kutsun välja meetodi

        // Leia diagonaal sentimeetrites
//        firstmonitor.diagonalCm();

        System.out.printf("Diagonaal on %.1f cm %n", maxSizeMonitor.getDiagonalToCm());
        System.out.printf("Diagonaal on %.1f cm %n", firstmonitor.getDiagonalToCm());

        // leia diagonaal meetrites
    }


}
