package it.vali;

public class Main {

    public static void main(String[] args) {
	// Funktsioon/meetod
        //Stringi funktsioonid
        //Sümbolite indeksid tekstis algavad samamoodi ineksiga 0 nagu massiivides
        //Leia üles esimene tühik, mis on tema indeks

        String sentence = "Elaskord metsas Mutionu keset kuuski noori vanu";
        int firstSpaceIndex = sentence.indexOf(" ");
        System.out.println(firstSpaceIndex); //4, tühiku indeks
        int onuIndex = sentence.indexOf("onu");
        System.out.println(onuIndex); // 16, o tähe indeks

        // indeksOf tagastab -1, kui otsitavat fraasi/sümbolit/sõna ei leitud
        // ning indeksi (kust sõna algab) kui fraas leitakse

        //Kuidas saada kätte teine tühik
        int secondSpaceIndex = sentence.indexOf(" ", firstSpaceIndex + 1);
        System.out.println(secondSpaceIndex);

        System.out.println();
        // Prindi välja kõigi tühikute indeksid
        int allSpaceIndex = sentence.indexOf(" ");
        while (allSpaceIndex != -1) {
            System.out.println(allSpaceIndex); //prindib esimese tühiku, läheb järmisesse plokki ja prindib järgmise
            allSpaceIndex = sentence.indexOf(" ",allSpaceIndex + 1);
        }

        System.out.println();
        // Prindi välja kõigi tühikute indeksid tagantpoolt
        allSpaceIndex = sentence.lastIndexOf(" ");
        while (allSpaceIndex != -1) {
            System.out.println(allSpaceIndex); //prindib esimese tühiku, läheb järmisesse plokki ja prindib järgmise
            allSpaceIndex = sentence.lastIndexOf(" ",allSpaceIndex - 1);
        }

        System.out.println();
        //Prindi mulle välja lause 4 esimest tähte
        String part = sentence.substring(0, 4); //kust alates ja kuhu maani (c# on kust alates ja mitu tähte)
        System.out.println("Lause esimesed 4 tähte on: " + part);

        System.out.println();
        // anna mulle lause esimene sõna (ma pikkust ei tea)
//        String firstWord = sentence.substring(0, sentence.indexOf(" "));
        String firstWord = sentence.substring(0, firstSpaceIndex);
        System.out.println("Esimene sõna on: " + firstWord);

        System.out.println();
        // anna mulle lause teine sõna
//        String secondWord = sentence.substring(firstSpaceIndex + 1, secondSpaceIndex);
        String secondWord = sentence.substring(sentence.indexOf(" ") + 1, sentence.indexOf(" ", sentence.indexOf(" ")+1));
        System.out.println("Teine sõna on: " + secondWord);

        System.out.println();
        // Leia esimene K - tähega algav sõna
        int KIndex  = sentence.indexOf(" k") + 1;
        int spaceIndex = sentence.indexOf(" ", KIndex);//otsib alates kIndeksist järgmist tühikut

        String KWord = sentence.substring(KIndex, spaceIndex);
        System.out.println("Esimene k-tähega sõna on: " + KWord);

        System.out.println();

        // Leia esimene K - tähega algav sõna
        // et leiaks ka siis, kui see sõna on lause esimene sõna
        // ja mis siis kui ei ole k tähega sõna

        String firstLetter = sentence.substring(0, 1);
        KWord = " ";
        if (firstLetter.equals("k".toUpperCase())) {
            spaceIndex = sentence.indexOf(" "); // esimene tühik
            KWord =sentence.substring(0, spaceIndex);
        }
        else {
            KIndex  = sentence.indexOf(" k")+1;
            if(KIndex != 0) {
                spaceIndex = sentence.indexOf(" ", KIndex); // esimene tühik pärast sõna
                KWord = sentence.substring(KIndex, spaceIndex);
            }
        }
        if (KWord.equals(" ")){
            System.out.println("Puudub k-tähega algav sõna");
        }
        else {
            System.out.println("esimene k-tähega algav sõna on: " + KWord);
        }


        //Leia mitu sõna mul lauses on

        allSpaceIndex = sentence.indexOf(" ");
        int spaceCounter = 0;
        while (allSpaceIndex != -1) {

            allSpaceIndex = sentence.indexOf(" ", allSpaceIndex + 1);
            spaceCounter++;
        }
        System.out.println("Sõnade arv lauses on: " + spaceCounter);


        //Leia mitu k-tähega algavat sõna on lauses

        int counter = 0;
        sentence = sentence.toLowerCase();
        firstLetter = sentence.substring(0, 1); // kust alates kuhu maani
        int KWords = sentence.indexOf(" k"); // otsin esimese ktähega algava sõna => sentence.indexOf(" k", 0)
        if (firstLetter.equals("k") ) {
            counter++;
        }
        while (KWords != -1){
            KWords = sentence.indexOf(" k", KWords +1 ); // esimesest sõnast alates otsin teist sõna - tühik +1 => alates k-st otsib tühik k-d
            counter++;
        }
        System.out.println("K-tähega algavate sõnade arv lauses on: " + counter);


    }
}
