package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    System.out.println("Sisesta arv");

        Scanner scanner = new Scanner (System.in);
        int number = Integer.parseInt(scanner.nextLine());

        if (number > 3) {
            System.out.println("Number on suurem kui 3");
        }
        else if (number == 0) { // peab olema eespool kui väiksem kui 3, sest kui see on õige, siis edasi enam ei lähe
            System.out.println("Number on 0");
        }
        else if (number < 3) {
            System.out.println("Number on väiksem kui 3");
        }
        //else käib vaid omale eelneva if-ga koos, seetõttu tuleb eelnev kirjutada else if

        else {
            System.out.println("Numbrid on võrdsed");
        }
    }
}
