package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Küsitakse külastaja nime
        // kui nimi on listis, siis öeldakse kasutajale "Tere tulemast Lauri!"
        // ja küsitakse külastaja vanust
        // Kui kasutaja on alaealine, teavitatakse teda, et sorry sisse ei saa
        // muuljuhul tere tulemast klubisse

        // Kui kasutaja ei olnud listis
        // Küsitakse kasutajalt ka tema perekonnanimi
        // kui perekonna nimi on listis, siis öeldakse tere tulemast Siiri Kivits
        // muul juhul öeldakse ma ei tunne sind

        String firstnameList ="Siiri";
        firstnameList = firstnameList.toLowerCase();
        String lastnameList = "Liis";
        lastnameList = lastnameList.toUpperCase();

        Scanner scanner = new Scanner (System.in);

        System.out.println("Mis on sinu eesnimi?");
        String firstname = scanner.nextLine(); //= scanner.nextLine().toLowerCase()
        firstname = firstname.toLowerCase();

        if (firstname.equals(firstnameList)) {
            System.out.printf("Tere tulemast %s ! Mis on sinu vanus?%n", firstname);
            int age = Integer.parseInt(scanner.nextLine());

            if (age < 18) {
                System.out.println("Oled alaealine, sina sisse ei saa");
            }
            else {
                System.out.println("Tere tulemast klubisse!");
            }
        }
        else {
            System.out.println("Kuidas on perekonnanimi?");
            String lastname = scanner.nextLine();
            lastname = lastname.toUpperCase();
            if (lastname.equals(lastnameList)) {
                System.out.printf("Tere tulemast klubisse %s %s!%n", firstname, lastname);
            }
            else {
                System.out.println("Ma ei tunne sind");
            }
        }

    }
}
