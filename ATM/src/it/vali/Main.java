package it.vali;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Scanner;

public class Main {


    static String pin;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        boolean checkPin = validatePin();
        int balance = loadBalance();
        boolean invalid = false;


        do {
            do {

                if (checkPin == false) {
                    System.out.println("Kaart on konfiskeeritud");
                    return;
                }
                else {
                    System.out.println("Vali toiming:\n " +
                            "a) Sularaha sissemakse \n" +
                            "b) Sularaha väljamakse \n" +
                            "c) Kontojääk\n" +
                            "d) Paroolivahetus\n" +
                            "e) Lõpeta\n");

                    String input = scanner.nextLine();

                    input = input.toLowerCase();
                    switch (input) {
                        case "a":
                                System.out.println("Sisesta raha");
                                int amount = Integer.parseInt(scanner.nextLine());
                                balance = loadBalance();
                                balance += amount;
                                saveBalance(balance);
                                System.out.printf("Sinu kontole lisati %d ja jääk on %d %n", amount, balance);
                                break;
                        case "b":

                                System.out.println("Vali summa: \n" +
                                        "a) 5 eur \n" +
                                        "b) 10 eur \n" +
                                        "c) 20 eur \n" +
                                        "d) Muu summa\n");

                                String howMuch = scanner.nextLine();
                                howMuch = howMuch.toLowerCase();

                                switch (howMuch){
                                    case "a":
                                        if (5 < loadBalance()) {
                                            balance = balance - 5;
                                            saveBalance(balance);
                                            System.out.printf("Sinu kontolt võeti %d ja jääk on %d %n", 5, balance);

                                        }
                                        else {
                                            System.out.println("Kontol pole piisavalt vahendeid");
                                        }
                                        break;
                                    case "b":
                                        if (10 < loadBalance()) {
                                            balance = balance - 10;
                                            saveBalance(balance);
                                            System.out.printf("Sinu kontolt võeti %d ja jääk on %d %n", 10, balance);
                                        }
                                        else {
                                            System.out.println("Kontol pole piisavalt vahendeid");
                                        }
                                        break;
                                    case "c":
                                        if (20 < loadBalance()) {
                                            balance = balance - 20;
                                            saveBalance(balance);
                                            System.out.printf("Sinu kontolt võeti %d ja jääk on %d %n", 20, balance);
                                        }
                                        else {
                                            System.out.println("Kontol pole piisavalt vahendeid");
                                        }
                                        break;
                                    case "d":
                                        System.out.println("Palun sisestage väljavõetav summa");
                                        int cashOut = Integer.parseInt(scanner.nextLine());
                                        if (cashOut < loadBalance()) {
                                            balance = balance - cashOut;
                                            saveBalance(balance);
                                            System.out.printf("Sinu kontolt võeti %d ja jääk on %d %n", cashOut, balance);
                                        }
                                        else {
                                            System.out.println("Kontol pole piisavalt vahendeid");
                                        }
                                        break;
                                }
                                break;

                            case "c":
                                balance = loadBalance();
                                System.out.printf("Sinu kontojääk on %d%n", balance);
                                break;

                            case "d":
                                checkPin = validatePin();
                                if (checkPin == true) {
                                    System.out.println("Sisesta uus PIN kood");
                                    String newPin = scanner.nextLine();
                                    savePin(newPin);
                                    System.out.println("PIN kood muudetud.");
                                }
                                else {
                                    System.out.println("Parooli muutmine ebaõnnestus. Teie kaart konfiskeeriti");
                                    return;

                                }
                                break;

                            case"e":
                                System.out.println("Ilusat päeva!");
                                return;



                            default:
                                System.out.println("Selline tehe puudub.");
                                invalid = true;

                                break;

                    }
                }

            }
            while (invalid);
            System.out.println("Kas sa soovid jätkata? Jah/Ei");



        } while (scanner.nextLine().toLowerCase().equals("jah"));
        System.out.println("Ilusat päeva!");
    }

    // Toimub pin  kirjutamine pin.txt faili
    static void savePin(String pin) {
        try {
            FileWriter fileWriter = new FileWriter("pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Pin koodi salvestamine ebaõnnestus.");
        }
    }

    // Toimub pin välja lugemine pin.txt failist
    static String loadPin() {

        try {
            FileReader fileReader = new FileReader("pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();

        } catch (IOException e) {
            System.out.println("Pin koodi lugemine ebaõnnestus");
        }
        return pin;
    }

    static boolean validatePin () {
        Scanner scanner = new Scanner(System.in);
        pin = loadPin();
        for (int i = 0; i < 3 ; i++) {
            System.out.println("Palun sisesta PIN kood");
            String enteredPin = scanner.nextLine();

            if (enteredPin.equals(pin)) {
                System.out.println("PIN on õige");
                return true;

            }
            else {
                System.out.println("Sisestatud PIN on vale");
            }
        }
        return false;
    }



    // Toimub balance kirjutamine balance.txt faili
    static void saveBalance(int balance) {

        try {
            FileWriter fileWriter = new FileWriter("balance.txt");
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus.");
        }
    }

    // Toimub balance välja lugemine balance.txt failist
    static int loadBalance() {

        int balance = 0;
        try {
            FileReader fileReader = new FileReader("balance.txt", Charset.forName("Cp1252"));
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = Integer.parseInt(bufferedReader.readLine());
            bufferedReader.close();
            fileReader.close();

        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");
        }
        return balance;
    }

}

//         Scanner scanner = new Scanner(System.in);
//         int balance = loadBalance();
//
//         String pin = loadPin();
//         boolean pinCorrect = true;
//         for (int i = 0; i <3 ; i++) {
//             System.out.println("Sisesta PIN:");
//             String InputPin = scanner.nextLine();
//             if (InputPin.equals(pin)) {
//                 System.out.println("Pin õige \n");
//                 break;
//             }
//             else {
//                 System.out.println("PIN on vale");
//                 pinCorrect = false;
//             }
//         }
//         if (pinCorrect !=true) {
//             System.out.println("Sisestaisd PIN koodi 3 korda valesti.");
//             System.exit(0);
//         }
//
//
//        do {
//            do {
//
//                System.out.println("Vali toiming: " +
//                        "a)sularaha sissemakse,\n" +
//                        "b)sularaha väljamakse,\n" +
//                        "c)kontojääk, \n" +
//                        "d)muuda pin kood)");
//
//                String input = scanner.nextLine();
//                input = input.toLowerCase();
//
//                switch (input) {
//                    case "a":
//                        System.out.println("Sisesta raha");
//                        int amount = Integer.parseInt(scanner.nextLine());
//                        balance = loadBalance();
//                        balance += amount;
//                        saveBalance(balance);
//                        System.out.printf("Sinu kontole lisati %d ja jääk on %d %n", amount, balance);
//                        break;
//                    case "b":
//                        System.out.println("Vali summa: " +
//                                "a) 5 eur " +
//                                "b) 10 eur " +
//                                "c) 20 eur " +
//                                "d) Muu summa");
//                        String money = scanner.nextLine();
//                        money = money.toLowerCase();
//                        break;
//                    case "c":
//                        balance = loadBalance();
//                        System.out.printf("Sinu kontojääk on %d%n", balance);
//                        break;
//
//                    case "d":
//                        System.out.println("Sisesta vana pin");
//                        if (loadPin().equals(scanner.nextLine())) {
//                            System.out.println("Vali uus pin");
//                            String newPin = scanner.nextLine();
//                            savePin(newPin);
//                            System.out.println("Pin edukalt muudetud");
//                        }
//                        break;
//
//                    default:
//                        System.out.println("Selline tehe puudub.");
//                        break;
//
//                }
//
//            }
//            while (true);
//        }
//            while (true);
//
//
//    }
//



