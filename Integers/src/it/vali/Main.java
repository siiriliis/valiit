package it.vali;

public class Main {

    public static void main(String[] args) {

	    // muutujat deklareerida ja talle väärtust anda saab ka kahe sammuga
	    //String word;
	    //word ="Kala";

        int number;
        number = 3;

        int secondNumber = -7;
        int summa = number + secondNumber;
        int korrutis = number * secondNumber;

        System.out.println(number);
        System.out.println(secondNumber);

        //int summa = number + secondNumber;
        //System.out.println("Summa on: " + summa);
        System.out.println(number+secondNumber);

        //System.out.println("Arvude 3 ja -7 korrutis on: " + number*secondNumber);
        //System.out.printf("Arvude %d ja %d korrutis on: %d \n",number, secondNumber, korrutis);

        //%n saab kasutada\n asemel reavahetuseks. %n tekitab platvormi - spetsiifilise reavahetuse
                //(Windows \r\n ja Linux/Mac \n)
                // Selle asemel saab kasutada ka System.lineSeparator()
                    //System.out.printf("Arvude %d ja %d korrutis on: %d%s ",number,
                    // secondNumber, number*secondNumber,System.lineSeparator());   System.lineSeparator() - teeb ka reavahetuse
        System.out.printf("Arvude %d ja %d korrutis on: %d%n",number, secondNumber, number * secondNumber);

        int a = 3;
        int b = 14;
        //String + number on alati string
        //Stringi liitmisel numbriga, teisendatatkse number stringiks ja liidetakse kui liitsõna
                // System.out.println("Arvude summa on " + a + b);
        System.out.println("Arvude summa on " + (a + b));

        System.out.printf("Arvude %d ja %d jagatis on: %d%n", a, b, a / b);
        System.out.printf("Arvude %d ja %d jagatis on: %d%n", b, a, b / a);
        // Kahe täisarvu jagamisel on tulemus jagatise täisosa ehk kõik peale koma süüakse ära

        int maxInt = 2147483647;
        int c = maxInt + 1;
        int d = maxInt + 2;
        int e = maxInt + maxInt;
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
        // kui max numbrile liita juurde, hakkab ta uuesti min miinusest lugema (ehk hakkab ringi tegema)
        int minInt = -2147483648;
        int f = minInt - 1;
        System.out.println(f);

        short k = 32199;

        long m = 9999999999999999L; // lõppu tuleb panna l täht (võib väike kui suur olla, suur on loetavam
        long n = 345;
        System.out.println(m + n);

    }
}
