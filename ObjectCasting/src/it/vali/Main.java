package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        //casting -> int ei mahu shorti sisse
        int a = 100;
        //short b =a;
        short b = (short) a;

        a = b; // b mahub a sisse

        double c = a;
        a = (int) c;

        // klasside castimine

        //iga kassi võib võtta kui looma
        // implicit casting
        Animal animal = new Cat();

        // iga loom ei ole kass
        // explicit casting
        //Cat cat = new Animal(); // niipidi teha ei saa
        Cat cat = (Cat)animal;

        // saan teha array listi tüübist animals
        // saan hoida ühes listis erinevaid loomi (kassid, koerad, lehmad on kõik loomad)

        List<Animal> animals = new ArrayList<Animal>();
        Dog dog = new Dog();
        dog.setName("Naki");
        Cow cow = new Cow();
        Pig pig = new Pig();
        Bear bear = new Bear();

        animals.add(dog);
        dog.setName("Muki");

        animals.add(cow);
        animals.add(pig);
        animals.add(bear);
        animals.add(new Wolf()); // ei pea muutujat tegema, võin otse ka luua objekti
        animals.get(animals.size()-1).setWeight(50);
        animals.add(new Pet());
        ((Pet)animals.get(animals.size()-1)).setHasHome(true);
        animals.get(animals.size()-1).setName("Kiisu"); // anname indeksiga nime, kui ei ole muutujale nime andnud

        animals.get(1).setName("Mati"); // saab ka indeksiga nime anda

        animals.get(animals.indexOf(dog)).setName("Murri"); // küsin kõige pealt koera indeksi, siis ma küsin välja koera .get ja siis panen nime.setName
        // kutsu kõikide listis olevate loomade printInfo välja
        for (Animal animalInList: animals) { //Klass nimi: kollektsioon(siin list)
            animalInList.printInfo(); //kuigi on list Animal, kutsub välja ka alamklasside printinfo
            System.out.println();
        }

        for (Animal animalInList: animals) { //Klass nimi: kollektsioon(siin list)
            animalInList.eat(); //kuigi on list Animal, kutsub välja ka alamklasside printinfo
            System.out.println();
        }

        // kui tahan printida kõik pet alamklassid (kass, koer), siis selleks on reflection

        Animal secondAnimal = new Dog();  // jäävad alles aint animal meetodid
        //secondAnimal.  // => ei anna koera meetodeid (saba ja mängin kassiga)
        // kirjuta, et koeral on saba või ei
        // pean teisendama tagasi
        ((Dog)secondAnimal).setHasTail(true); // nüüd on secondAnimal dog
        secondAnimal.printInfo();




    }
}
