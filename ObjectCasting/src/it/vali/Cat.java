package it.vali;

public class Cat extends Pet{ // kasutades extendi, siis ühiseid omadused on vaja kirjutada vaid animals klassi

    private boolean hasFur = true; // vaikimisi false

    // get ja set on vajalikud selleks, et main-is saaks kirjutada
    // Cat angora = new Cat();
    // angora.age(10), => nii ei saa, sest see on private
    // aga nüüd saab panna angora.setAge(10)

    public boolean isHasFur() {
        return hasFur;
    }

    public void setHasFur(boolean hasFur) {
        this.hasFur = hasFur;
    }


    // meetod kass püüab hiiri
    public void catchMouse () {
        System.out.println("Püüdsin hiire kinni");
    }
    //@Override
    public void eat() {
        System.out.println("Söön hiiri");
    }
    public void printInfo() {
        super.printInfo();
        System.out.printf("Karvade olemasolu: %s%n", hasFur);

    }


}
