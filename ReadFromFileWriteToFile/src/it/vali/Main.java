package it.vali;

import java.io.*;
import java.nio.charset.Charset;

public class Main {

    public static void main(String[] args) {
        // Loe failist input.txt iga teine rida ja kirjuta need read faili output.txt

        try {
            // Notepad vaikimisi kasutab Ansi encodingut.
            // Selleks, et fileReader oskaks seda korretkselt lugeda (täpitähti),
            // peame talle ette ütlema, et loe ANSI encodin-us
            // Cp1252 on javas ANSI encoding
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt", Charset.forName("Cp1252")); // kust failist loeme
            BufferedReader bufferedReader = new BufferedReader(fileReader); // loeme reakaupa
            String line = bufferedReader.readLine(); // loeme esimese rea sisse
            // faili kirjutades on javas vaikeväärtus UTF-8
            FileWriter fileWriter = new FileWriter("kirjutame.txt", Charset.forName("UTF-8")); //kuhu kirjutame. Salvestame UTF -8 vormingusse ja edasi oskab õigesti la lugeda

            int lineNumber = 1;

            while (line != null) { // kuni rida pole tühi loeb ridu ja kirjutab need faili "kirjutame"
                if (lineNumber % 2 == 1) {
                    fileWriter.write(line + System.lineSeparator()); // kirjutab rea ja vahetab rida
                }
                line = bufferedReader.readLine(); // uus rida
                lineNumber++;

            }
            fileReader.close(); // lugeja tuleb kinni panna
            fileWriter.close(); // kirjutaja tuleb kinni panna
        } catch (IOException e) {
            e.printStackTrace();
        }



    }
}
