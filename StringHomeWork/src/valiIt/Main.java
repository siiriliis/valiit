package valiIt;

public class Main {

    public static void main(String[] args) {
        //Mitu tähte on sõnas
        String word = "kala";
        int length = word.length(); //annab sõna tähtede arvu
        System.out.println("Sõnas on: " + length + " tähte");

        String[] words = new String[] {"Põdral", "maja", "metsa", "sees"};
        System.out.println();
        // Prindi kõik sõnad

        for (int i = 0; i < words.length ; i++) {
            System.out.println(words[i]);
        }

        System.out.println();
        // Prindi kõik sõnad massiivist, mis algavad m -tähega
        for (int i = 0; i < words.length ; i++) {
            String firstLetter = words[i].substring(0, 1); //alates esimesest indeksist
            if (firstLetter.toLowerCase().equals("m")) {
                System.out.println(words[i]);
            }
        }

        System.out.println();
        // Prindi kõik a tähega lõppevad sõnad
        for (int i = 0; i < words.length ; i++) {
            String lastLetter = words[i].substring(words[i].length() - 1); //alates viimasest indeksist (nt pikkus 10, index 9)
            if (lastLetter.toLowerCase().equals("a")) {
                System.out.println(words[i]);
            }
        }


        //Loe üle kõik sõnad, mis sisaldavad a tähte
        //variant 1
        int counter = 0;
        for (int i = 0; i < words.length; i++) {
            int a = words[i].indexOf("a");
            if (a != -1) {
                counter++;
            }
        }
        System.out.println(counter);
        //variant 2
        counter = 0;
        for (int i = 0; i < words.length; i++) {
            if(words[i].contains("a")){
                counter++;
            }
        }
        System.out.println(counter);

        //Loe üle kõik sõnad kus on 4 tähte
        counter = 0;
        for (int i = 0; i < words.length ; i++) {
            if (words[i].length() == 4) {
                counter++;
            }
        }
        System.out.println(counter);

        // Prindi välja kõige pikema sõna pikkus
        int longest = words[0].length();
        for (int i = 0; i < words.length; i++) {
            if (words[i].length() > longest) {
                longest = words[i].length();
            }
        }
        System.out.println(longest);

        // Prindi välja kõige pikem sõna

        String longestWord = words[0];
        for (int i = 0; i < words.length; i++) {
            if (words[i].length() > longestWord.length()) {
                longestWord = words[i];
            }
        }
        System.out.println(longestWord);

        //Prindi välja sõnad, kus on esimene ja viimane täht on sama

        for (int i = 0; i < words.length ; i++) {
            String lastLetter = words[i].substring(words[i].length() - 1); //alates viimasest indeksist
            String firstLetter = words[i].substring(0, 1); //alates esimesest indeksist

            if (lastLetter.toLowerCase().equals(firstLetter.toLowerCase())) {
                System.out.println(words[i]);
            }
        }

    }
}

