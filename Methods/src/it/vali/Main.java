package it.vali;

public class Main {
    // Meetodid on mingid koodi osad,
    // mis grupeerivad mingit teatud kindlat funktsionaalsust
    //Kui koodis on korduvaid koodi osasi, võiks mõelda,
    // et äkki peaks nende kohta tegema eraldi meetodi

    //meetodi väljakutsumine
    public static void main(String[] args) {
        printHello(); //kõik mida defineerin klassi sees on näha kõigis meetodites
        printHello(10);
        printText("mis teed");
        printText("õpin", 5);
        printText("ilus ilm", 5, true);
    }

    // Lisame meetodi, mis prindib ekraanile Hello
    private static void printHello() { // sulgudesse saab panna parameetri
        // kui public ette ei pane, siis vaikimisi on private;
        // static => paneme kuna ei ole objektorjenteeritud
        // void => midagi ei tagastata

        System.out.println("Hello");
    }

    // Lisame meetodi, mis prindib Hello etteantud arv kordi
    static void printHello (int howManyTimes) { //static void = private static void
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println("Hello");
        }

    }
    //Lisame meetodi, mis prindib enneantud teksti välja
    static void printText(String text) {
        System.out.println(text);
    }
    //Lisame meetodi, mis prindib ette antud teksti välja ette antud arv kordi
    static void printText(String text, int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println(text);
        }
    }
    //Method OVERLOADING => meil on mitu meetodit sama nimega, aga erinevate parameetrite kombinatsiooniga

    //Lisame meetodi, mis prindib ette antud teksti välja ette antud arv kordi.
    // Lisaks saab öelda, kas tahame teha kõik tähed enne suureks või mitte
    static void printText (String text, int howManyTimes, boolean toUpperCase) {
        for (int i = 0; i < howManyTimes; i++) {
            if (toUpperCase) {
                System.out.println(text.toUpperCase());
            } else {
                System.out.println(text);
            }
        }

    }

}
