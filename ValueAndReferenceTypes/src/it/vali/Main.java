package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // Kui panna üks primitiivne (value type) tüüpi muutja võrduma teise muutujaga,
        // siis tegelikult tehakse arvuti mällu uus muutuja ja väärtus kopeeritakse sinna
        // sama juhtub ka primitiiv (value type) tüüpi muutuja kaasa andmisel meetodi parameetriks
        // (tegelikult tehakse arvuti mällu uus muutuja ja väärtus kopeeritakse sinna)

        // eeldusel, et mälus on kohad 1-10:
        int a = 3; // asub mälus nt kohal 1
        int b = a; // asub mälus kohal 2
        a = 7;

        System.out.println(b);// b = 3
        increment(b);// b = 4
        System.out.println(b); // b = 3

        // Kui panna reference- type (viit -(viitavad) tüüpi muutja võrduma teise muutujaga,
        // siis tegelikult jääb mälus ikkagi alles ainult 1 muutuja,
        // lihtsalt teine muutuja hakkab viitama samale kohale mälus ( samale muutujale)
        // meil on kaks muutujat (numbers, secondNumbers),
        // aga tegelikult on nad täpselt sama objekt
        // Sama juhtub ka Reference-type viit tüüpi muutuja kaasa andmisel meetodi parameetriks

        int [] numbers = new int [] {-5, 4}; // asub mälus kohal 3 ja 4
        int []  secondNumbers = numbers; // asub mälus kohal 3 ja 4
        numbers[0] = 3; // asub mälus kohal 3
        System.out.println(secondNumbers[0]); // mälus kohal 3 on 3

        // klassist teeme objekti/instance-i (konkreetne point klassist Point
        Point pointA = new Point();
        pointA.x = 10;
        pointA.y = 3;

        Point pointB = pointA;
        pointB.x = 7;
        System.out.println(pointA.x); // 7, sest kirjutab mälusse samasse kohta pointA ja pointB,
        // ehk kui pointB kordinati muudan, muudan ka pointA kordinaati



        List<Point>points = new ArrayList<Point>();
        points.add(pointA);
        points.add(pointB);

        pointA.y = -4;

        System.out.println(points.get(0).y);
        System.out.println(points.get(1).y);
        System.out.println(pointB.y);
        System.out.println();

        // staatiline või mittestaatiline?
        Point.printStatic(); // staatiline
        System.out.println(); //staatiline, ma ei tee selle jaoks objekti

        pointA.printNotStatic(); // ei ole staatiline
        int o = Integer.parseInt("3"); // staatiline

        Point pointC = new Point ();
        pointC.x = 12;
        pointC.y = 20;
        increment(pointC);
        System.out.println(pointC.x);

        int[] thirdNumbers = new int [] {1,2,3};
        increment(thirdNumbers);
        System.out.println(thirdNumbers[0]);

        Point pointD = new Point();
        pointD.x = 5;
        pointD.y = 6;
        pointD.increment();
        System.out.println(pointD.y);

    }

    // static tähendab, et ma ei pea objekti looma, et meetodit välja kutsuda

    // suurendame a-d 1 võrra
    public static void increment(int a) {
        a++;
        System.out.printf("a on nüüd %d%n", a);
    }
    // suurendame x ja y koordinaate 1 võrra
    public static void increment (Point point) {
        point.x++;
        point.y++;
    }
    // suurendame kõiki elemente 1 võrra
    public static void increment (int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            numbers[i]++;
        }

    }
}
