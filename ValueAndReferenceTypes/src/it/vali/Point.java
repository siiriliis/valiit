package it.vali;

public class Point {
    // kapseldamise võib välja jätta, kui seal hoida kordinaate
    public int x;
    public int y;


    //staticu ja mitte staticu vahe
    // staatiline meetod ei ole seotud objektiga
    public static void printStatic () {
        System.out.println("Olen static punkt");   // Point.printStatic();
    }

    // on seotud konktreetse objektiga, näiteks pointA
    public void printNotStatic () {
        System.out.println("Mina olen ka punkt"); // pointA.printNotStatic();
    }

    //instance meetod ehk klassi enda meetod (ilma staticuta)
    public void increment () {
        x++;
        y++;

    }

}
