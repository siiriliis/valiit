package com.hellokoding.account.repository;  
import java.io.ByteArrayInputStream;
import java.sql.ResultSet;  
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.support.lob.DefaultLobHandler;

import com.hellokoding.account.model.Department;
import com.hellokoding.account.model.Emp;  
  
public class EmpDao {  
JdbcTemplate template;  
  
public void setTemplate(JdbcTemplate  template) {  
    this.template = template;  
}  
public int save(Emp p){  
	MapSqlParameterSource mapSelParameterSource = new MapSqlParameterSource();
	mapSelParameterSource.addValue("name", p.getName());
	mapSelParameterSource.addValue("salary", p.getSalary());
	mapSelParameterSource.addValue("designation", p.getDesignation());
	mapSelParameterSource.addValue("picture",  new SqlLobValue(new ByteArrayInputStream(p.getPicture()), 
	   p.getPicture().length, new DefaultLobHandler()), Types.BLOB);
	   NamedParameterJdbcTemplate jdbcTemplateObject = new 
		         NamedParameterJdbcTemplate(template.getDataSource());
	   
	   
	   
	   mapSelParameterSource.addValue("departmentId", p.getDepartmentId());
	   
	String sql = "insert into Emp99(name,salary,designation,picture, departmentId) "
			+ "VALUES (:name, :salary, :designation, :picture, :departmentId)";
  
    return jdbcTemplateObject.update(sql, mapSelParameterSource);  
}  
public int update(Emp p){  
    String sql="update Emp99 set name='"+p.getName()+"', salary="+p.getSalary()+",designation='"+p.getDesignation()+"',departmentId="+p.getDepartmentId() +" where id="+p.getId()+"";  
    return template.update(sql);  
}  
public int delete(int id){  
    String sql="delete from Emp99 where id="+id+"";  
    return template.update(sql);  
}  
public Emp getEmpById(int id){  
    String sql="select * from Emp99 where id=?";  
    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Emp>(Emp.class));  
}  
public List<Emp> getEmployees(){  
    return template.query("select id,name,salary,designation from Emp99",new RowMapper<Emp>(){  
        public Emp mapRow(ResultSet rs, int row) throws SQLException {  
            Emp e=new Emp();  
            e.setId(rs.getInt(1));  
            e.setName(rs.getString(2));  
            e.setSalary(rs.getFloat(3));  
            e.setDesignation(rs.getString(4)); 
            return e;  
        }  
    });  
}  


public List<Department> getDepartments(){  
    return template.query("select id, name from department",new RowMapper<Department>(){  
        public Department mapRow(ResultSet rs, int row) throws SQLException {  
            Department department=new Department();  
            department.setId(rs.getInt(1));  
            department.setName(rs.getString(2));  
            return department;  
        }  
    });  
} 



}  