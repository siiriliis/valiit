package it.vali;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt");
            //fileReader.read(); // loeb vaid ühe sümboli kaupa

            BufferedReader bufferedReader = new BufferedReader(fileReader); // loeb reakaupa

            // readLine loeb esimese rea, mis pole veel loetud. Loeb iga kord välja kutsudes järgmise rea
            String line = bufferedReader.readLine(); // loeb esimese rea
            //System.out.println(line); // prindib esimese rea
//
//            line = bufferedReader.readLine(); // loeb teise rea
//            System.out.println(line);
//
//            line = bufferedReader.readLine(); // loeb kolmanda rea
//            System.out.println(line);

            // kuidas teha seda While tsükliga:
            // kui readLine avastab, et järgmist rida tegelikult ei ole, siis tagastab meetod null
            while (line != null) {// null tähendab tühjust ehk järgmist rida pole. tühi rida teksti vahel on "" mitte null
                System.out.println(line);
                line = bufferedReader.readLine();
            }

            //Kuidas teha seda do while
//            line = null;
//            do {
//                line = bufferedReader.readLine();
//                if(line!=null) {
//                    System.out.println(line);
//                }
//            } while (line != null);

            bufferedReader.close();
            fileReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
