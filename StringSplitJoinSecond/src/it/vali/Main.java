package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Split tükeldab Stringi ette antud sümbolite kohalt ja tekitab sõnade massiivi
	// String split poolitab lause/sõna. On võimalik teha stringist stringi massiivi. nt kirjutades ilm, lõikab sõna ilm vahelt ära
        String sentence = "Väljas on ilus ilm ja päike paistab ";
        String[] words = sentence.split("ilm"); // regulaaravaldis sõna või sõna osa
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]); // tagastab Väljas on ilus
                                        //ja päike paistab
        }
        System.out.println();
        words = sentence.split(" ", 3); // võtab ära 3 esimest tühkut
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }
        System.out.println();
        sentence = "Väljas on ilus ilm, vihma ei saja ja päike paistab ";
        words = sentence.split(" ja | |, "); // | tähendab regulaaravaldises või
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]); // tagastab  sõnad eraldi ridadele ilma ja ja komata
        }

        // Join
        String newSentence = String.join(" ja ",words); // esimene parameeter läheb sõnadele vahele, teine on massiivi nimi
        System.out.println(newSentence); // tagastab Väljas ja on ja ilus ja ilm ja vihma ja ei ja saja ja päike ja paistab

        newSentence = String.join("\n",words); //eraldajaks on reavahetus
        System.out.println(newSentence);

        // Lauses jutumärkide kasutamine
        //Escaping
        System.out.println("Juku ütles: \\n \"Mulle meeldib suvi\""); //Kui tahan, et prindiks ka \n siis selle ette veel üks \
        System.out.println("\\Users\\opilane\\Documents\\GitHub\\valiit"); // \ tuleb panna topelt
        System.out.println("Juku\b ütles: \\n \"Mulle meeldib suvi\""); // \b kustutab ära "u" tähe Juku lõpust

        // Küsi kasutajalt rida numbreid nii, et ta paneb need numbrid kirja ühele reale eraldades tühikuga.
        // Seejärel liidab need kõik numbrid kokku ja prindib vastuse

        System.out.println("sisesta arvud, mida tahad omavahel liita, eraldades tühikuga");
        Scanner scanner = new Scanner (System.in);
        String numbersText = scanner.nextLine();
        String[] numberList = numbersText.split(" ");
        int sum = 0;
        String newNumbersText = String.join(", ", numberList);
        for (int i = 0; i < numberList.length; i++) {
            int numbers = Integer.parseInt(numberList[i]);
            sum = sum + numbers;
            //sum = sum + Integer.parseInt(numberList[i]);
            //sum += Integer.parseInt(numberList[i]);
        }
        //System.out.println(sum);

        System.out.printf("Arvude %s summa on %d%n", newNumbersText, sum);
    }
}
