package it.vali;

public class Main {

    static int a =3;
    public static void main(String[] args) {

        int b = 7;
        System.out.println( b + a); // 10

        a = 0; // muudab klassi a = 3 a =0iks
        System.out.println( b + a); // 7
        System.out.println(increaseByA(10)); // 10

        // Shadowing, ehk siis sees pool defineeritud muutuja
        // varjutab väljaspool oleva muutuja
        int a = 6;
       //Meetodis defineeritu a muutmine
        a = 3;
        // Klassi defineeritud a muutmine.
        Main.a = 4;
        System.out.println( b + a); // 10
        System.out.println(increaseByA(10)); // 14
        System.out.println(b + Main.a); //11
        System.out.println (b + a); //10



    }

    static int increaseByA (int b) {
       // int a = 4;
        return b = b + a;
    }
}
