package it.vali;

public class Main {

	public static void main(String[] args) {
		int a = 21;

		// Kui arv on 4, siis prindi välja arv on 4
		// muul juhul kui arv on neg, siis kontrolli kas arv on suurem kui -10,
		// muul juhul kontrolli kas arv on suurem kui 20
		if (a == 4) {
			System.out.println("Arv on 4");
		}
		else {
			if (a < 0) {
				if (a < -10) {
					System.out.println("Arv on negatiivne aga suurem kui -10");
				}
			}
			else {
				if (a > 20) {
					System.out.println("Arv on suurem kui 20");
				}
			}
		}
	}
}
