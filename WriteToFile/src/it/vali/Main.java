package it.vali;

import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        //FileWriter filewriter = new FileWriter("output.txt"); //c# lubab sellist koodi
        // try plokis otsitakse/ oodatatkse Exceptioneid
        // (Erind; Erand; Viga - erijuht, mida saab lahendada)

        try { //punane lamp => surround try/catch => tekib selline try/catch blokk
            // FileWriter on selline klass, mis kirjutab faili
            // sellest klassist objekti loomisel öeldakse talle ette faili asukoht
            // faili asukoht võib olla ainult faili nimega kirjeldatud (output.txt)
            // sel juhul kirjutatakse faili, mis asub samas kaustas, kus asub meie Main.class
            // või täispika asukohaga ("e:\\users\\opilane\\documents\\output.txt") => kirjutab dokumendi kausta siis
            FileWriter filewriter = new FileWriter("C:\\Users\\opilane\\Documents\\output.txt");
            // fileWriteri sisse vaatamiseks => parem klahv => go to => declaration

            for (int i = 0; i < 3 ; i++) {
                filewriter.write("Elas metsas mutionu\r\n"); // notepadi jaoks reavahetus \r\n
                filewriter.write("keset kuuski noori vanu" + System.lineSeparator()); // nii töötab linuxis kui windowsis
                filewriter.write(String.format("kadak põõõsa juure all" )); //ka võimalus

            }

            filewriter.close(); // alati tuleb ära ka lõpetada, muidu ei salvesta


            // catch plokis püütakse kinni kindalt tüüpi exceptionid
            // või kõik exceptionid, mis pärinevad antud exceptionist =>
            // => (https://docs.oracle.com/javase/8/docs/api/java/io/IOException.html  =>
            // => IOExceptionist pärinevad Exceptionid  Direct Known Subclasses
        } catch (IOException e) {
            // printStackTrace => prinditakse välja meetodite
            // välja kutsumise hierarhia/ajalugu :
                        // java.io.FileNotFoundException: e:\output.txt (The device is not ready)
                        // at java.base/java.io.FileOutputStream.open0(Native Method)
                        // at java.base/java.io.FileOutputStream.open(FileOutputStream.java:292)
                        // at java.base/java.io.FileOutputStream.<init>(FileOutputStream.java:235)
                        // at java.base/java.io.FileOutputStream.<init>(FileOutputStream.java:124)
                        // at java.base/java.io.FileWriter.<init>(FileWriter.java:66)
                        // at it.vali.Main.main(Main.java:11)
//            e.printStackTrace(); // lõppkasutajale seda näidata ei tohiks,
                //    tuleb välja kommenteerida ja System.out.println tekst kirjutada =>
                //    häkker saaks siit vajaliku info kätte
            System.out.println("Viga: antud failile ligipääs ei ole võimalik"); // kasutaja näeb, et on viga, aga mis viga ei näe kui printStackTrace on välja kommenteeritud
        }
    }
}
