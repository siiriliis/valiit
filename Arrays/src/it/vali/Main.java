package it.vali;

public class Main {

    public static void main(String[] args) {
	    // järjend, massiiv, nimekiri, array
        // luuakse täisarvude massiiv, millesse mahub 5 elementi
        // loomise hetkel määratud elementide arvu hiljem muuta ei saa.
        int[] numbers = new int [5]; // 5 on elementide arv
        // massiivi indeksid algavad 0st mitte 1st
        // viimane indeks on alati 1 võrra väiksem kui massiivi pikkus
        numbers[0] = 2;
        numbers [1] = 7;
        numbers[2] = -2;
        numbers[3] = 11;
        numbers [4] = 1;

//        System.out.println(numbers[0]);
//        System.out.println(numbers[1]);
//        System.out.println(numbers[2]);
//        System.out.println(numbers[3]);
//        System.out.println(numbers[4]);

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        System.out.println();

        // prindi numbrid tagurpidises järjekorras
        for (int i = numbers.length -1 ; i >= 0; i--) {
            System.out.println(numbers[i]);
        }
        System.out.println();

        // Prindi numbrid, mis on suuremad kui 2
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > 2) {
                System.out.println(numbers[i]);
            }
        }
        System.out.println();
        // Prindi kõik paarisarvud
        for (int i = 0; i < 5; i++) {
            if (numbers[i] % 2 == 0) {
                System.out.println(numbers[i]);
            }
        }
        System.out.println();

        //Prindi tagantpoolt 2 esimest paaritut arvu
        int counter = 0;
        for (int i = numbers.length -1 ; i >= 0; i--) { // numbers.length -1 = 4
            if (numbers[i] % 2 != 0) {
                System.out.println(numbers[i]);
                counter++;
                if (counter == 2) {
                    break;
                }
            }
        }
        System.out.println();

        // Loo teine massiiv 3-le numbrile ja pane sinna esimesest massiivist 3 esimest numbrit
        // ja prindi teise massiivi elemendid ekraanile (2, 7, -2)
        int[] newNumbers = new int [3];
        for (int i = 0; i <3 ; i++) {
            newNumbers[i] = numbers[i];
//            System.out.println(newNumbers[i]);
        }
        for (int i = 0; i <3 ; i++) {
            System.out.println(newNumbers[i]);
        }
        System.out.println();

        // Loo kolmas massiiv 3-le numbrile ja
        // pane sinna esimesest massiivist 3 viimast numbrit tagant poolt alates (1, 11, -2)
        int[] thirdNumbers = new int [3];
        for (int i = 0; i < 3; i++) {
//            thirdNumbers [0] = numbers [4];
//            thirdNumbers [1] = numbers [3];
//            thirdNumbers [2] = numbers [2];

            thirdNumbers[i] = numbers[numbers.length - i - 1];
            System.out.println(thirdNumbers[i]);
        }
        System.out.println();


        // juhul kui me ei mõtle valemit välja, kuidas siis seda teha
        int[] fourthNumbers = new int[3];
        for (int i = 0, j = numbers.length-1; i < fourthNumbers.length; i++, j--) {
            fourthNumbers[i] = numbers[j];
        }
        for (int i = 0; i < fourthNumbers.length; i++) {
            System.out.println(fourthNumbers[i]);
        }

        System.out.println();
        // Loo neljas massiiv 3-le numbrile ja
        // pane sinna esimesest massiivist 3 viimast numbrit tagant poolt alates üle ühe

        for (int i = 0, j = numbers.length-1; i < fourthNumbers.length; i++, j-=2) {
            fourthNumbers[i] = numbers[j];
        }
        for (int i = 0; i < fourthNumbers.length; i++) {
            System.out.println(fourthNumbers[i]);
        }
    }
}
