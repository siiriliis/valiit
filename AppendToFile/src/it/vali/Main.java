package it.vali;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        try {
            FileWriter fileWriter = new FileWriter("output.txt", true); // lisades true =>
                        // igakord käivitades kirjutab ühe rea tere juurde
            fileWriter.append("Tere\r\n");

            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 1. koosta täisarvude massiiv 10st sõnast ning seejärel kirjuta faili kõik suuremad arvud kui 2
        int [] numbers = new int[] {1,2,3,4,5,6,7,8,9,10} ;
        try {
            FileWriter filewriter = new FileWriter("numbers.txt"); // See ei tohiks for tsükli sees olla

            for (int i = 0; i <numbers.length ; i++) {
                if (numbers[i] > 2) {
                    String arv = String.valueOf(numbers[i]);
                    filewriter.write(arv + "\r\n"); // write korral kirjutab igakord üle, append korral kirjutab juurde
                }
            }
            filewriter.close(); // peab olema väljaspool for tsüklit

        } catch (Exception e) {
            System.out.println("Viga: antud failile ligipääs ei ole võimalik");
        }

        // 2. küsi kasutajalt 2 arvu .
        // Küsi seni kuni mõlemad on korrektsed (on number)
        // ning nende summa ei ole paarisarv


        Scanner scanner = new Scanner (System.in);
        boolean answer;
        int firstInput = 0;
        int secondInput = 0;
        boolean sum = false;
        do {
            do {
                try {
                    answer = false;
                    System.out.println("Palun sisesta esimene arv");
                    firstInput = Integer.parseInt(scanner.nextLine());

                } catch (NumberFormatException e) {
                    answer = true;
                    System.out.println("See ei ole number");
                }
            } while (answer);

            do {
                try {
                    answer = false;
                    System.out.println("Palun sisesta teine arv");
                    secondInput = Integer.parseInt(scanner.nextLine());

                } catch (NumberFormatException e) {
                    answer = true;
                    System.out.println("See ei ole number");
                }
            } while (answer);

        }
        while ((firstInput+secondInput) %2 != 0);



        // 3. Küsi kasutajalt mitu arvu ta tahab sisestada.
        // Seejärel küsi arve ühekaupa.
        // Ning kirjuta nende arvude summa faili nii, et see lisatakse alati juurde

        System.out.println("Mitu arvu soovid sisestada?");
        int count = Integer.parseInt(scanner.nextLine());
        int summa = 0;
        for (int i = 0; i < count ; i++) {
            System.out.printf("Sisesta arv number %d%n",i+1);
            int number = Integer.parseInt(scanner.nextLine());
            summa += number;
        }
        try {
            FileWriter fileWriter = new FileWriter("summa.txt", true); // lisades true =>
            // igakord käivitades kirjutab ühe rea tere juurde
            fileWriter.append(summa + "\r\n"); // fileWriter.append(summa + System.lineSeparator) => siis töötab linuxis ka

            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }






}
