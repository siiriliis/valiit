package it.vali;

public class Main {

    public static void main(String[] args) {
        // koma tuleb panna punktina
	    float a = 123.34f; // java tahab f tähte lõppu
        double b = 123.4343; // double on default ja siin ta tähte taha ei taha

        System.out.printf("arvude %f ja %f summa on %f \n", a, b, a + b ); //%f ei ümarda?
        System.out.printf("arvude %e ja %e summa on %e \n", a, b, a + b ); //%e annab kümme astmed
        System.out.printf("arvude %g ja %g summa on %g \n\n", a, b, a + b ); //%g ümardab vähemtäpsema järgi

        System.out.printf("arvude %g ja %g korrutis on %g \n", a, b, a * b );
        System.out.printf("arvude %g ja %g jagatis on %g \n\n", a, b, a / b );

        float c = 130.4f;
        float e = 9.3f;

        System.out.printf("float korral arvude %g ja %g jagatis on %g \n", c, e, c / e );
        System.out.println(c/e);

        double f = 130.4;
        double g = 9.3;

        System.out.printf("double korral arvude %g ja %g jagatis on %g \n", f, g, f / g );
        System.out.println(f/g); //doublel on täpsus komakohtades suurem kui floatil kui kasutada println käsku

        System.out.println(c*e); //float lühendab ja ümardab
        System.out.println(f*g); //double on täpsem

    }
}
