package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        //1. Arvuta ringi pindala, kui teada on raadius
        //  Prindi pindala välja erkaanile
        double radius = 10;
        // Math on staatiline meetod
        //double area = Math.PI * Math.pow(radius,2); // .pow raadius, astmes 3
        double area = circleArea (radius);
        System.out.printf("Ringi pindala on %.1f%n",area);
        //System.out.println(circleArea(10));
        System.out.printf("Ringi pindala on %.2f%n",circleArea(10));

        // 2. ül
//        System.out.println(isEqual("auto", "auto"));
//        System.out.println(isEqual("auto", "kass"));
        String x = "kala";
        String y = "kala";
        boolean areEqual = isEqual(x,y);
        System.out.printf("Sõnad %s ja %s on võrdsed: %s%n", x,y,areEqual);
        System.out.printf("Sõnad %s ja %s on võrdsed: %s%n", x,y,isEqual(x,y));

        // 3. ül

        int[] numbers = new int[] {3, 6, 7};
        String[] words = intArrayToStringArray(numbers);

        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }


        // 4. ül
        List<Integer> years = leapYearsInCentury(400);
        for (int year:years) {
            System.out.println(years);
        }
//        for (int year: leapYearsInCentury(1897)) {
//            System.out.println(year);
//        }


        // 5. Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning list riikide nimedega
        // kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString() nii, et see tagastab
        // riikide nimekirja eraldades komaga.
        // Tekita antud klassist 1 objekt ühe vabalt valitud keele andmetaga ning prindi välja
        // selle objekti toString() meetodi sisu.

        Language language = new Language();
        language.setLanguageName("English");
        List<String> countryNames = new ArrayList<String>();
        countryNames.add("USA");
        countryNames.add("Canada");
        countryNames.add("UK");


        language.setCountryNames(countryNames);
        System.out.println(language);






    }
    // 1. Arvuta ringi pindala, kui teada on raadius
    // Prindi pindala välja erkaanile

    static double circleArea(double radius) {
        return  Math.PI * Math.pow(radius,2);

    }
    // 2. Kirjuta meetod, mis tagastab boolean tüüpi väärtuse
    // ja mille sisendparameetriteks on kaks stringi.
    // Meetod tagastab kas tõene või vale selle kohta,
    // kas stringid on võrdsed



    static boolean isEqual (String x, String y) {
        if (x.equals(y)) {
           // System.out.printf("Sõna %s ja %s on samad sõnad %n", x, y);
            return true;
        }
        return false;


    }
    // 3. Kirjuta meetod, mille sisendparameetriks on täisarvude
    // massiiv ja mis tagastab stringide massivi.
    // Iga sisend-massiivi elemendi kohta olgu tagastatavas massiivis samapalju a tähti.
    // 3, 6, 7
    // "aaa", "aaaaaaa", "aaaaaaa"

    static String [] intArrayToStringArray  (int [] numbers) {
        //numbers = new int[] {1,2,3,4};
        String [] words  = new String[numbers.length];
        //        numbers[0] = 3; words[0] = "aaa";
        //        numbers[1] = 4; words[1] = "aaaa";
        //        numbers[2] = 7; words[2] = "aaaaaaa";

        for (int i = 0; i < numbers.length ; i++) {
            words [i] = generateAString (numbers[i]);
        }
        return words;
    }
    static String generateAString(int count) {
        String word = "";
        for (int i = 0; i < count; i++) {
            word = word + "a";

        }
        return word;
    }




    // 4. Kirjuta meetod, mis võtab sisendaparameetrina aastaarvu
    // ja tagastab kõik sellel sajandil esinenud liigaastad
    // Sisestada saab ainult aastaid vahemikus 500-2019.
    // Ütle veateade, kui aastaarv ei mahu vahemikku.
    static List<Integer> leapYearsInCentury (int year) {
        List<Integer> years = new ArrayList<Integer>(); // teeb tühja listi
        if (year <500 || year > 2019) {
            System.out.println("Aasta peab olema 500 ja 2019 vahel");
            return years; // tagastab tühja listi
        }

        int centuryStart = year/ 100 * 100;
        int centuryEnd = centuryStart +99;
        int leapYear = 2020;

        for (int i = 2020; i >= centuryStart; i -= 4) {
            if ( i <= centuryEnd && i % 4==0) {
                years.add(i); // toimub listi lisamine
            }
        }
        return years;
    }
}
