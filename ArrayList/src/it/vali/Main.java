package it.vali;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        // teeme int massiivi
        int [] numbers = new int[5]; // teen "tühja" massiivi, mis tegelikult ei ole tühi, sees on nullid
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]); // prindib nullid, sest massiivi pannakse vaikeväärtused

        }

        System.out.println();
        // asenda massiivis nullid numbritega
        numbers[0] = 4;
        numbers[1] = 5;
        numbers[2] = 0;
        numbers[3] = -6;
        numbers[4] = 4;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        System.out.println();

        // Tavalisse arraylisti võin lisada ükskõik mis tüüpi elemente,
        // aga elemente küsides sealt pean ma teadma,
        // mis tüüpi element kus täpselt asub.
        // Ning pean siis selleks tüübiks küsimisel ka cast-ima
        List list = new ArrayList();

        list.add("Tere");
        list.add(23);
        list.add(false);
        double money = 24.55;
        Random random = new Random();
        list.add(money);
        list.add(random);

        for (int i = 0; i < list.size() ; i++) { //list.size küsib listi pikkust nagu array puhul .length
            System.out.println(list.get(i));
        }
        System.out.println();

        // tuleb kasutada castimist
        int a = (int) list.get(1);
        System.out.println(a);
        String word = (String) list.get(0);
        System.out.println(word);

        //int sum = 3 + a;
        int sum = 3 + (int)list.get(1);
        System.out.println(sum);

        String sentence = list.get(0) + " hommikust!";
        System.out.println(sentence);

        if (Integer.class.isInstance(a)) {
            System.out.println("a on int");

        }
        if( String.class.isInstance(list.get(0))){
            System.out.println("listis indeksiga 0 on string");
        }
        if( Integer.class.isInstance(list.get(1))){
            System.out.println("listis indeksiga 1 on int");
        }
        list.add(2, "head aega");

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println();

        // Lisame listile teise listi
        List otherList = new ArrayList();
        otherList.add(1);
        otherList.add("maja");
        otherList.add(3);
        otherList.add(true);

        list.addAll(otherList);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        if(list.contains(money)) {
            System.out.println("money asub listis");
        }
        else {
            System.out.println("money ei asu listis");
        }
        System.out.printf("money asub listis indeksiga %d%n", list.indexOf(money));
        System.out.printf("23 asub listis indeksiga %d%n", list.indexOf(23));
        System.out.printf("23 asub listis asukohaga nr %d%n", list.indexOf(23)+1);
        //list.isEmpty(); // kas list on tühi

        // eemaldame listist money
        list.remove(money);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println();
        // eemaldame listist elemndi indeksiga 5
        list.remove(4);
        System.out.println(list.size());
        list.set(0, "tore");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }






    }
}
