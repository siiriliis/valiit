package it.vali;

public class Main {

    public static void main(String[] args) {
	    int[] numbers = new int [] {16, 4, 10, 15, 14, 6};

        // Leia max nr massiivist
//        int max = numbers[0];
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < numbers.length ; i++) {
            if (numbers[i] > max) { // võrdlen, kas järgmine nr on suurem kui max, kui on siis max = on uus max nr
                max = numbers[i];
            }
        }
        System.out.println("Suurim number on: " + max);


        System.out.println();
        // Leia min nr massiivist
//        int min = numbers[0];
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < numbers.length ; i++) {
            if (numbers[i] < min) {
                min = numbers[i];
            }
        }
        System.out.println("Vähim number on: " + min);


        System.out.println();
        // Leia suurim paaritu arv
        // mitmes number see on järjekorras
        int maxOdd = Integer.MIN_VALUE;
        int position = 1; // indeks kohal 1 on 0. Koha jaoks on indeks +1
        for (int i = 0; i < numbers.length; i++) {

            if (numbers[i] % 2 != 0 && numbers [i] > maxOdd) {
                maxOdd = numbers[i];
                position = i+ 1;
            }
        }
//        System.out.println("Suurim paaritu arv on " + maxOdd + " ja selle järjekorra number on " + position );
        System.out.printf("Suurim paaritu arv on %d ja selle järjekorra number on %d %n", maxOdd, position );

        System.out.println();
        // Kui paarituid arve ei ole, prindi, et "Paaritud arvud puuduvad"

        //variant 1
        maxOdd = Integer.MIN_VALUE;
        position = 1; // indeks kohal 1 on 0. Koha jaoks on indeks +1
        boolean oddNumbersFound = false;

        for (int i = 0; i < numbers.length; i++) {

            if (numbers[i] % 2 != 0 && numbers [i] > maxOdd) {
                maxOdd = numbers[i];
                position = i+ 1;
                oddNumbersFound = true;
            }

        }
        if(oddNumbersFound) {
            System.out.printf("Suurim paaritu arv on %d ja selle järjekorra number on %d %n", maxOdd, position );
        }
        else {
            System.out.println("Paaritud arvud puuduvad");
        }
        System.out.println();


        //variant 2
        maxOdd = Integer.MIN_VALUE;
        position = -1;
        for (int i = 0; i < numbers.length; i++) {

            if (numbers[i] % 2 != 0 && numbers [i] > maxOdd) {
                maxOdd = numbers[i];
                position = i+ 1;
            }

        }
        if (position != -1) {
            System.out.printf("Suurim paaritu arv on %d ja selle järjekorra number on %d %n", maxOdd, position );
        }
        else {
            System.out.printf("Paaritud arvud puuduvad %n" );
        }
     }
}
